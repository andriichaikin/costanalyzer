import gql from "graphql-tag";

export const GET_ALL_PROJECTS_QUERY = gql`
    query getAllProjects{
        projects{
            id name budget spent saldo status kpi deadline comment
        }
    }
`;

export const GET_ALL_PROJECTS_NAMES_QUERY = gql`
    query getAllProjectsNames {
        projects {
            id name
        }
    }
`;

export const GET_PROJECT_BY_ID_QUERY = gql`
    query projectById($id: Int!) {
        project(id: $id) {
            id, name, budget, spent, saldo, status, kpi, deadline, comment
        }
    }
`;

export const GET_PROJECT_NAME_QUERY = gql`
    query projectName($prId: Int!) {
        project(id: $prId) {
            name
        }
    }
`;

export const GET_DETAILED_REPORT_QUERY = gql`
    query reportDetailedEmployee($projectId: Int!, $from: Int!, $to: Int!){
        reportDetailedProject(projectId: $projectId, from: $from,to: $to) {
            employees {
                employeeId, name, surname, earnedMoney, hours
            }
        }
    }
`;

export const GET_DETAILED_REPORT_EMPLOYEE_KPI_QUERY = gql`
    query reportDetailedEmployeeKPI($projectId: Int!, $employeeId: Int!,  $from: Int!, $to: Int!){
        reportDetailedProjectEmployee(
            dailyDetailedInput:{projectId: $projectId, employeeId: $employeeId, from: $from, to: $to}) {
            weekdays{
                hours, count, earnedMoney
            },
            holidays {
                hours, count, earnedMoney
            }
        }
    }
`;

export const GET_BRIEF_REPORT_QUERY = gql`
    query getBriefReports($from: Int!, $to: Int!) {
        reportBriefProjects(from: $from, to: $to) {
            id, name, spent
        }
    }
`;

export const GET_SYSTEM_USER = gql`
    query getSystemUser($id: Int!) {
        systemUser(id: $id){
            id, name, surname, email, phone
        }
    }
`;

export const GET_ALL_EMPLOYEES_NAMES_QUERY = gql`
    query getAllEmployeesNames {
        employees {
            id, name, surname
        }
    }
`;

export const GET_EMPLOYEE_BY_ID_QUERY = gql`
    query employeeById($id: Int!) {
        employee(id: $id) {
            id, name, surname, salary, position
        }
    }
`;