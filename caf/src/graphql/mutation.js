import gql from 'graphql-tag';

export const UPDATE_SYS_USER_MUTATION = gql`
    mutation updateSystemUser($id: Int!, $name: String!, $surname: String!, $email: String!, $phone: String!){
        updateSystemUser(systemUserInput:{id: $id, name: $name, surname: $surname, email: $email, phone: $phone}) {
            id, name, surname, email, phone
        }
    }
`;

export const CREATE_PROJECT_MUTATION = gql`
    mutation createProject(
        $name: String!, $budget: Int!, $spent: Int!, $saldo: Int!,
        $status: String!, $deadline: String!, $comment: String){
        createProject(projectInput: {
            name: $name,
            budget: $budget,
            spent: $spent,
            saldo: $saldo,
            status: $status,
            deadline: $deadline,
            comment: $comment
        }) {
            id, name, budget, spent, saldo, status,  kpi, deadline, comment
        }
    }
`;

export const UPDATE_PROJECT_MUTATION = gql`
    mutation updateProject(
        $id: ID!, $name: String!, $budget: Int!, $spent: Int!, $saldo: Int!,
        $status: String!, $deadline: String!, $comment: String){
        updateProject(projectInput: {
            id: $id,
            name: $name,
            budget: $budget,
            spent: $spent,
            saldo: $saldo,
            status: $status,
            deadline: $deadline,
            comment: $comment
        }) {
            id, name, budget, spent, saldo, status,  kpi, deadline, comment
        }
    }
`;

export const CREATE_EMPLOYEE_MUTATION = gql`
    mutation createEmployee($name: String!, $surname: String!, $salary: Int!, $position: String!){
        createEmployee(employeeInput: {name: $name, surname: $surname, salary: $salary, position: $position}){
            id, name, surname, salary, position
        }
    }
`;

export const UPDATE_EMPLOYEE_MUTATION = gql`
    mutation updateEmployee($id: ID!, $name: String!, $surname: String!, $position: String!, $salary: Int!) {
        updateEmployee(employeeInput: {id: $id, name: $name, surname: $surname, position: $position, salary: $salary}) {
            id, surname, name, position, salary
        }
    }
`;