import axios from 'axios';

const BASE_URL = 'http://192.168.0.112:5000/api';

const API = axios.create({
    baseURL: BASE_URL,
    withCredentials: true,
});

export default API;