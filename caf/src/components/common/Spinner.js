import React from 'react';
import './Spinner.scss';

const Spinner = props => {
    return <div className="lds-dual-ring"></div>;
};

const SpinnerWrapper = ({classNames, ...props}) => {

    return (
        <div className={classNames}>
            <Spinner {...props} />
        </div>
    );
};

export default SpinnerWrapper;