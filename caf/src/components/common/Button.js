import React from 'react';

import './Button.scss';

const Button = ({onClick, type = 'button', ...props}) => {

    return <button type={type}
                   className='btn btn-border'
                   onClick={onClick}>{props.children}</button>
};

const ButtonWrapper = ({classNames = '', ...props}) => {
    return <div className={classNames}>
        <Button {...props}>{props.children}</Button>
    </div>
};

export default ButtonWrapper;