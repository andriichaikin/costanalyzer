import React from 'react';

import './Bookmarks.scss';

export const BOOKMARKS = {project: 'PROJECT', employee: 'EMPLOYEE'};

const Bookmarks = ({mode, changeBookmark}) => {

    const handleClick = bookmark => {
        changeBookmark(bookmark);
    };

    const {project, employee} = BOOKMARKS;
    const activeBookmarkStyle = ' bookmarks__item--active';
    return (
        <div className='bookmarks'>
            <ul className='bookmarks__items'>
                <li className={`bookmarks__item${project === mode ? activeBookmarkStyle : ''}`}
                    onClick={handleClick.bind(null, project)}>
                    <div className='bookmarks__block'>
                        <span className='bookmarks__title'>Project</span>
                    </div>
                </li>
                <li className={`bookmarks__item${employee === mode ? activeBookmarkStyle : ''}`}
                    onClick={handleClick.bind(null, employee)}>
                    <div className='bookmarks__block'>
                        <span className='bookmarks__title'>Employee</span>
                    </div>
                </li>
            </ul>
        </div>
    )
};

export default Bookmarks;