import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
// import MomentLocaleUtils, {formatDate, parseDate} from 'react-day-picker/moment';
// import 'moment/locale/uk';

import Button from "./Button";
import "react-day-picker/lib/style.css";
import './DualDaysPicker.scss';

const DualDaysPicker = ({from, to, onDayChangeFrom, onDayChangeTo, onBtnClick, btnName}) => {

    const handleFromDayChange = (selectedDay, modifiers, dayPickerInput) => {
        onDayChangeFrom(selectedDay, modifiers, dayPickerInput);
    };

    const handleToDayChange = (selectedDay, modifiers, dayPickerInput) => {
        onDayChangeTo(selectedDay, modifiers, dayPickerInput);
    };

    const handleBtnClick = () => {
        onBtnClick();
    };

    return (
        <div className='dual-days__container'>
            <div className='dual-days__input-block'>
                <DayPickerInput
                    onDayChange={handleFromDayChange}
                    selectedDay={from}
                    // dayPickerProps={{
                    //     selectedDays: from,
                    //     localeUtils: MomentLocaleUtils,
                    //     locale: 'en',
                    // }}
                    // formatDate={formatDate}
                    // parseDate={parseDate}
                    // placeholder={`${formatDate(new Date())}`}/>
                    placeholder='from...'/>

                <span className='dual-days__arrow'>-></span>

                <DayPickerInput
                    onDayChange={handleToDayChange}
                    selectedDay={to}
                    // dayPickerProps={{
                    //     selectedDays: to,
                    //     localeUtils: MomentLocaleUtils,
                    //     locale: 'en',
                    // }}
                    // formatDate={formatDate}
                    // parseDate={parseDate}
                    // placeholder={`${formatDate(new Date())}`}/>
                    placeholder='to...'/>
            </div>
            <Button onClick={handleBtnClick}>{btnName}</Button>
        </div>
    );
};

const DualDaysPickerWrapper = ({classNames, ...props}) => {
    return (
        <div className={classNames}>
            <DualDaysPicker {...props}/>
        </div>
    )
};

export default DualDaysPickerWrapper;