import React from 'react';

import './CirclePercentage.scss';

const CirclePercentage = ({text, percent, colorStyle = {}}) => {

    return (
        <svg viewBox="0 0 36 36" className='circular-chart'>
            <path className="circle-bg"
                  d="M18 2.0845
                            a 15.9155 15.9155 0 0 1 0 31.831
                            a 15.9155 15.9155 0 0 1 0 -31.831"/>
            <path className='circle circle--default-color'
                  style={colorStyle}
                  strokeDasharray={`${percent}, 100`}
                  d="M18 2.0845
                          a 15.9155 15.9155 0 0 1 0 31.831
                          a 15.9155 15.9155 0 0 1 0 -31.831"/>
            <text className="percentage" x="18" y="22">{text}</text>
        </svg>
    )
};

const CirclePercentageWrapper = ({classnames, ...props}) => {

    return <div className={classnames}><CirclePercentage {...props}/></div>

};

export default CirclePercentageWrapper;
