import React from 'react';
import './BlurredText.scss';

const BlurredText = ({onToggleBlur, id, isBlurred, ...props}) => {

    const isBlurredStyles = isBlurred === true ? 'blurred-text__content--visible' : 'blurred-text__content--blurry';

    const toggleBlurred = (e) => {
        const {id} = e.currentTarget.dataset;
        onToggleBlur(id);
    };

    return (
        <div className={`blurred-text--wrapper`}
             data-id={id}
             onClick={toggleBlurred}>
            <span className={`${isBlurredStyles}`}>{props.children}</span>
        </div>
    );
};

const BlurredTextWrapper = ({classNames, ...props}) => {

    return (
        <div className={classNames}>
            <BlurredText {...props}/>
        </div>
    );
};

export default BlurredTextWrapper;