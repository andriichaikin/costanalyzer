import React from 'react';

import dateFnsParse from "date-fns/parse";
import {DateUtils} from "react-day-picker";
import dateFnsFormat from "date-fns/format";
import DayPickerInput from "react-day-picker/DayPickerInput";
import FormControl from "./FormControl";

import "react-day-picker/lib/style.css";

function parseDate(str, format, locale) {
    const parsed = dateFnsParse(str, format, new Date(), {locale});
    return DateUtils.isDate(parsed) ? parsed : undefined;
}

function formatDate(date, format, locale) {
    return dateFnsFormat(date, format, {locale});
}

const FORMAT = 'dd/MM/yyyy';
const DateInputForm = props => {
    const {input, meta, child, date, ...rest} = props;

    let fDate = undefined;
    if (date) {
        const nDate = Date.parse(date);
        fDate = nDate ? dateFnsFormat(nDate, FORMAT) : undefined;
    }
    return (
        <FormControl {...props} >
            <DayPickerInput
                classNames={{container: 'form-control__day--container', overlay: 'form-control__day--overlay'}}
                value={fDate}
                inputProps={{...input}}
                selectedDay={input.value}
                formatDate={formatDate}
                format={FORMAT}
                parseDate={parseDate}
                placeholder={`${dateFnsFormat(new Date(), FORMAT)}`}/>
        </FormControl>
    )
};

export default DateInputForm;