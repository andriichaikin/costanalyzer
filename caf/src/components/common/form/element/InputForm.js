import React from 'react';
import FormControl from "./FormControl";

const InputForm = props => {

    const {input, meta, child, label, ...rest} = props;
    return (
        <FormControl {...props}>
            <input {...input} {...rest} placeholder=''/>
            <label htmlFor={input.name}>{label}</label>
        </FormControl>
    )
};

export default InputForm