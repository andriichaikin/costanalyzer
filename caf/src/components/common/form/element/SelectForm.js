import React from 'react';
import ReactSelect from "react-select";
import FormControl from "./FormControl";

const selectStyles = {
    container: (provided, state) => {
        const hasError = state && state.selectProps && state.selectProps.hasError;
        return {boxShadow: hasError ? '0 0 3px red' : '0 0 3px #145188'}
    },
    input: () => ({'input': {boxShadow: 'none !important'}})
};

const SelectForm = props => {
    const {input, meta, options} = props;
    const hasError = meta && meta.touched &&
        ((!Array.isArray(meta.error) && meta.error) || (Array.isArray(meta.error) && meta.error.length));

    return (
        <FormControl {...props}>
            <ReactSelect value={input.value || ''}
                         hasError={hasError}
                         styles={selectStyles}
                         options={options}
                         onChange={value => input.onChange(value)}
                         onBlur={() => input.onBlur(input.value)}/>
        </FormControl>
    )
};

export default SelectForm;