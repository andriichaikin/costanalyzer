import React from 'react';
import FormControl from "./FormControl";

const TextareaForm = props => {
    const {input, meta, child, ...rest} = props;
    return (
        <FormControl {...props}>
            <textarea {...input} {...rest} />
        </FormControl>
    )
};

export default TextareaForm