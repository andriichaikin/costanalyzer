import React from 'react';

import './FormControl.scss';

const FormError = ({error, isArray}) => {
    let content = '';
    if (isArray) {
        content = error[0];
        for (let i = 1; i < error.length; i++) {
            content = content.concat(' / ' + error[i]);
        }
    } else {
        content = error;
    }

    return (
        <div className='form-control__span-error'>
            <span>{content}</span>
        </div>
    )
};

const FormControl = ({input, meta, child, ...props}) => {
    const isArray = meta && Array.isArray(meta.error);
    const hasError = meta && meta.touched && ((meta.error && !isArray) || (isArray && meta.error.length));

    const errorStyles = hasError ? ' form-control--error' : '';

    return (
        <div className={`form-control${errorStyles}`}>
            {hasError && <FormError error={meta.error} isArray={isArray}/>}
            <div className='form-control__element'>
                {props.children}
            </div>
        </div>
    )
};

export default FormControl;