import parse from 'date-fns/parse'
import isValid from 'date-fns/isValid'

export const isRequired = value => (value || typeof value === 'number' ? undefined : 'Required');
const maxLength = max => value => value && value.length > max ? `Must be ${max} characters or less` : undefined;
export const isLengthGreater_50 = maxLength(50);
const minLength = min => value => value && value.length < min ? `Must be ${min} characters or more` : undefined;
export const isLengthLess_5 = minLength(5);
export const isNumber = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined;
export const isInteger = value => value && !Number.isInteger(value) ? 'Must be an integer' : undefined;
const minValue = min => value => value !== null && value !== undefined && value < min ?
    `Must be at least ${min}` : undefined;
const minValue_0 = minValue(0);
const minValue_1 = minValue(1);
export const isPositiveNumber = value => minValue_1(value) ? 'Must be positive number' : undefined;
export const isPositiveOrZero = value => minValue_0(value) ? 'Must be positive number or zero' : undefined;
export const containsLeadingZero = value =>
    !minLength(2)(value) && !isNumber(value) && Number(value[0]) === 0 ?
        'Number cannot starts with leading zero' : undefined;

const isDateDMy = format => value => {
    if(!value) return undefined;

    const date = parse(value, format, new Date());
    const isDate = Object.prototype.toString.call(date) === '[object Date]';
    return isDate ? undefined : `Invalid date format (should be ${format})`;
};
export const isDateDDMMyyyy = isDateDMy('dd/MM/yyyy');


export const invalidEmail = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : undefined;
export const invalidPhoneUA = value => value && value.length > 16 &&
!/^\+38\s0[1-9]{1}[0-9]{1}\s[0-9]{3}\s[0-9]{4}/.test(value) ? 'Invalid phone number' : undefined;

export const isAlphaNumeric = value =>
    value && /[^a-zA-Z0-9\r\n\s_-]/i.test(value) ? 'Only alphanumeric characters and symbols _ and -' : undefined;