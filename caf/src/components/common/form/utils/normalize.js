export const firstLetterUppercase = value => {
    if (!value) return value;

    if (typeof value !== 'string') return value;

    return value[0].toUpperCase() + value.slice(1, value.length);
};

export const toInteger = value => {
    if (!value) return value;

    const parsed = parseInt(value);
    if (isNaN(parsed)) return value;

    return parsed;
};

const uaIndx = '+38 0';
export const phoneFormatUA = value => {
    if (!value) return uaIndx;

    if (value.match(/.*\D$/)) return value.substring(0, value.length - 1);

    // let isCorrect = /^\+38\s0[1-9]{2}\s[0-9]{3}\s[0-9]{4}/.test(value);
    if (value.length > 16) return value.substring(0, 16);

    let payload = '';

    let found = value.match(/^\+?38?\s?0*|^8\s?0*|^0+/);
    const enteredIndx = found ? found[0] : null;

    if (enteredIndx) payload = value.substring(enteredIndx.length);
    else payload = value;

    payload = payload.replace(/\s+/g, '');

    const regExp = /^([1-9]{1}[0-9]{1})([0-9]{0,3})([0-9]{0,4})/;
    const format = '$1 $2 $3';
    payload = payload.replace(regExp, format);

    payload = payload.replace(/\s+$/g, '');
    return uaIndx.concat(payload);
};