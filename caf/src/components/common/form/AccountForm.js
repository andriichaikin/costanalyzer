import React, {Component} from 'react';

import './AccountForm.scss';
import Input from "./element/InputForm";
import {
    invalidEmail,
    isAlphaNumeric,
    isLengthGreater_50,
    isRequired,
    invalidPhoneUA,
    isInteger
} from "./utils/validate";
import {firstLetterUppercase, phoneFormatUA} from "./utils/normalize";
import {Field, reduxForm} from "redux-form";
import Button from "../Button";
import {connect} from "react-redux";

class AccountForm extends Component {

    render() {
        return <FormConnected onSubmit={this.props.handleSubmit} initialValues={this.props.user}/>
    }
}

export default connect(null, null)(AccountForm);

const Form = ({handleSubmit}) => {

    return (
        <form className='account-form' onSubmit={handleSubmit}>
            <Field name='name'
                   component={Input}
                   type='text'
                   label='Name'
                   validate={[isRequired, isAlphaNumeric, isLengthGreater_50]}
                   normalize={firstLetterUppercase}/>
            <Field name='surname'
                   component={Input}
                   type='text'
                   label='Surname'
                   validate={[isRequired, isAlphaNumeric]}
                   normalize={firstLetterUppercase}/>
            <Field name='email'
                   component={Input}
                   type='email'
                   label='Email'
                   validate={[isRequired, invalidEmail]}/>
            <Field name='password'
                   component={Input}
                   type='password'
                   label='Password'
                   validate={[isAlphaNumeric]}/>
            <Field name='phone'
                   component={Input}
                   type='tel'
                   label='Phone'
                   validate={[isRequired, invalidPhoneUA]}
                   normalize={phoneFormatUA}/>
            <Button type='Submit'>Update</Button>
        </form>
    )
};

const FormConnected = reduxForm({
    form: 'accountForm',
    enableReinitialize: true,
})(Form);