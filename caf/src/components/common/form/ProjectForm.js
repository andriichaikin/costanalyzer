import React, {Component} from 'react';
import {connect} from "react-redux";
import {change, Field, formValueSelector, reduxForm} from 'redux-form';

import DateInput from "./element/DateInputForm";
import Input from "./element/InputForm";
import Textarea from "./element/TextareaForm";
import Select from "./element/SelectForm";
import Button from "../Button";

import './ProjectForm.scss';
import {
    containsLeadingZero,
    isAlphaNumeric, isDateDDMMyyyy,
    isInteger,
    isLengthGreater_50,
    isLengthLess_5,
    isNumber, isPositiveNumber, isPositiveOrZero,
    isRequired
} from "./utils/validate";
import {firstLetterUppercase, toInteger} from "./utils/normalize";
import API from "../../../api/API";

export const PROJECT_FORM_MODE = {create: 'CREATE', edit: 'EDIT'};

const SALDO_INPUT = 'saldo';

class ProjectForm extends Component {

    asyncValidate = (values) => {
        // const errors = {};
        //
        // return API.get(`/project/find?name=${values.name}`)
        //     .then(res => {
        //         if (res.status === 200) throw {name: 'Name already exist'};
        //     });
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {budgetVal, spentVal, saldoVal, form, change} = this.props;
        const {budgetVal: budgetPrev, spentVal: spentPrev} = prevProps;

        if (budgetVal !== budgetPrev || spentVal !== spentPrev) {

            if (budgetVal === undefined || spentVal === undefined) {
                //optimize re-rendering
                if (saldoVal) change(form, SALDO_INPUT, '')
            } else if (isNaN(Number(budgetVal)) || isNaN(Number(spentVal))) {
                change(form, SALDO_INPUT, 'Incorrect number');
            } else {
                const saldo = budgetVal - spentVal;
                change(form, SALDO_INPUT, saldo);
            }
        }
    }

    handleSubmit = values => {
        const status = values && values.status && values.status.label;
        const comment = values && values.comment;

        const date = new Date(values.deadline);
        const deadline = date && date.getTime() && `${date.getTime()}`;

        return this.props.onSubmit({...values, status, comment, deadline});
    };

    render() {
        const {btnName, initialValues, form, mode} = this.props;

        const initialDeadline = initialValues && initialValues.deadline;
        return <ConnectedForm form={form}
                              enableReinitialize={PROJECT_FORM_MODE.edit === mode}
                              initialValues={initialValues}
                              date={initialDeadline}
                              mode={mode}
                              onSubmit={this.handleSubmit}
                              btnName={btnName}/>;  //todo mv from props to const
    }
}

const mapStateToProps = (state, {form}) => {
    const selector = formValueSelector(form);

    const budgetVal = selector(state, 'budget');
    let spentVal = selector(state, 'spent');
    let saldoVal = selector(state, SALDO_INPUT);

    return {budgetVal, spentVal, saldoVal};
};
const mapDispatchToProps = {change};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectForm);

const STATUS_OPTIONS = [
    {value: 'OPEN', label: 'OPEN'},
    {value: 'CLOSED', label: 'CLOSED'},
    {value: 'FREEZED', label: 'FREEZED'}
];
//todo fix date format bug
const Form = ({handleSubmit, btnName, mode, date}) => {

    return (
        <form className='form' onSubmit={handleSubmit}>
            <div className='form__row'>
                <Field name='name'
                       component={Input}
                       type='text'
                       label='Name'
                       validate={[isRequired, isAlphaNumeric, isLengthLess_5, isLengthGreater_50]}
                       normalize={firstLetterUppercase}/>
            </div>
            <div className='form__row'>
                <Field name='budget'
                       component={Input} type='text'
                       label='Budget'
                       validate={[isRequired, isNumber, isInteger, isPositiveNumber, containsLeadingZero]}
                       normalize={toInteger}/>
            </div>
            <div className='form__row'>
                <Field name='spent'
                       component={Input}
                       type='text'
                       label='Spent'
                       validate={[isRequired, isNumber, isInteger, isPositiveOrZero, containsLeadingZero]}
                       normalize={toInteger}
                       disabled={PROJECT_FORM_MODE.edit === mode}/>
            </div>
            <div className='form__row'>
                <Field name='saldo' component={Input} type='text' label='saldo' disabled/>
            </div>
            <div className='form__row'>
                <Field name='status'
                       component={Select}
                       type='text'
                       options={STATUS_OPTIONS}
                       label='Status'
                       validate={isRequired}/>
            </div>
            <div className='form__row'>
                <Field name='deadline'
                       component={DateInput}
                       date={date}
                       type='text'
                       label='Deadline'
                       validate={[isRequired, isDateDDMMyyyy]}/>
            </div>
            <div className='form__row'>
                <Field name='comment'
                       component={Textarea}
                       type='text'
                       label='Comment'
                       validate={[isAlphaNumeric]}/>
            </div>
            <Button type='submit'>{btnName}</Button>
        </form>
    )
};
const ConnectedForm = reduxForm({})(Form);