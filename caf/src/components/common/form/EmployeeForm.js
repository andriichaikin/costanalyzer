import React, {Component} from 'react';
import './EmployeeForm.scss';
import {Field, reduxForm, change} from "redux-form";
import {connect} from "react-redux";
import Input from "./element/InputForm";
import Select from "./element/SelectForm";
import {
    containsLeadingZero,
    isAlphaNumeric,
    isInteger,
    isLengthGreater_50,
    isNumber,
    isPositiveNumber,
    isRequired
} from "./utils/validate";
import {firstLetterUppercase, toInteger} from "./utils/normalize";
import Button from "../Button";

export const EMPLOYEE_FORM_MODE = {create: 'CREATE', edit: 'EDIT'};

class EmployeeForm extends Component {

    handleSubmit = values => {
        const position = values.position.value;
        this.props.onSubmit({...values, position, salary: +values.salary});
    };

    render() {
        const {form, mode, initialValues} = this.props;
        const btnName = EMPLOYEE_FORM_MODE.create === mode ? 'Save' : 'Update';

        return <ConnectedForm form={form}
                              enableReinitialize={EMPLOYEE_FORM_MODE.edit = mode}
                              initialValues={initialValues}
                              mode={mode}
                              onSubmit={this.handleSubmit}
                              btnName={btnName}/>
    }
}

const mapDispatchToProps = {change};

export default connect(null, mapDispatchToProps)(EmployeeForm);

const POSITION_OPTIONS = [
    {label: 'Engineer', value: 'Engineer'},
    {label: 'Programmer', value: 'Programmer'},
    {label: 'Designer', value: 'Designer'},
    {label: 'General', value: 'General'}
];
const Form = ({handleSubmit, btnName, mode}) => {

    return (
        <form className='form' onSubmit={handleSubmit}>
            <div className="form-row">
                <Field name='name'
                       component={Input}
                       type='text'
                       label='Name'
                       validate={[isRequired, isAlphaNumeric, isLengthGreater_50]}
                       normalize={firstLetterUppercase}/>
            </div>
            <div className="form-row">
                <Field name='surname'
                       component={Input}
                       type='text'
                       label='Surname'
                       validate={[isRequired, isAlphaNumeric, isLengthGreater_50]}
                       normalize={firstLetterUppercase}/>
            </div>
            <div className="form-row">
                <Field name='position'
                       component={Select}
                       options={POSITION_OPTIONS}
                       type='text'
                       label='Position'
                       validate={[isRequired]}/>
            </div>
            <div className="form-row">
                <Field name='salary'
                       component={Input}
                       type='text'
                       label='Salary'
                       validate={[isRequired, isNumber, isInteger, isPositiveNumber, containsLeadingZero]}
                       normalize={toInteger}/>
            </div>
            <Button type='submit'>{btnName}</Button>
        </form>
    )
};

const ConnectedForm = reduxForm({})(Form);