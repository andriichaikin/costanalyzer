import React from 'react';

import './Checkbox.scss';

const CheckBox = ({id, isChecked, onCheckBoxChange, label, ...props}) => {

    return (
        <div className='checkbox--wrapper'>
            <input className='checkbox__input'
                   id={id}
                   type="checkbox"
                   checked={isChecked}
                   onChange={onCheckBoxChange}
                   {...props}/>
            <label className='checkbox__label' htmlFor={id}>{label}</label>
        </div>
    )
};

const CheckBoxWrapper = ({classNames, ...props}) => {

    return <div className={classNames}>
        <CheckBox {...props}/>
    </div>
};

export default CheckBoxWrapper;