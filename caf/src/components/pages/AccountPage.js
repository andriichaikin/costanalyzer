import React from 'react';

import './AccountPage.scss';

import AccountForm from "../common/form/AccountForm";

import {graphql} from "react-apollo";
import {useMutation} from "@apollo/react-hooks";
import {GET_SYSTEM_USER} from "../../graphql/query";
import {UPDATE_SYS_USER_MUTATION} from "../../graphql/mutation";

const AccountPage = ({loading, errors, data}) => {

    const [updateSysUser, updated] = useMutation(UPDATE_SYS_USER_MUTATION);

    const handleSubmit = values => {
        const {id, name, surname, email, phone} = values;
        updateSysUser({
            variables: {
                id: +id, name, surname, email, phone
            }
        });
    };

    return (
        <div className='account-page'>
            <span style={{margin: '30px'}}>AccountPage</span>

            <div className='account-page__form--wrapper'>
                <AccountForm user={data.systemUser}
                             handleSubmit={handleSubmit}/>
            </div>
        </div>
    )
};

export default graphql(GET_SYSTEM_USER, {
    options: (props) => ({variables: {id: 1}})
})(AccountPage);