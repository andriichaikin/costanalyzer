import React, {Component} from 'react';

import './ReportBriefPage.scss';

import Checkbox from "../common/Checkbox";
import BlurredText from "../common/BlurredText";
import DualDaysPicker from "../common/DualDaysPicker";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHryvnia} from "@fortawesome/free-solid-svg-icons";
import {GET_BRIEF_REPORT_QUERY} from "../../graphql/query";
import {Query} from "react-apollo";

class ReportBriefPage extends Component {

    state = {
        from: null,
        to: null,
        selectedDatesSubmitted: '',
        isVisibleAllMoney: false,
        visibleMoneyList: [],
    };

    handleDayFromChange = (selectedDay, modifiers, dayPickerInput) => {
        dayPickerInput.getInput().focus();
        this.setState({from: selectedDay})
    };

    handleDayToChange = (selectedDay, modifiers, dayPickerInput) => {
        dayPickerInput.getInput().focus();
        this.setState({to: selectedDay})
    };

    toggleVisibleMoney = id => {
        const projectId = +id;

        const showed = [...this.state.visibleMoneyList];
        if (showed.includes(projectId)) {
            const indx = showed.indexOf(projectId);
            showed.splice(indx, 1);
        } else {
            showed.push(projectId);
        }

        this.setState({visibleMoneyList: showed});
    };

    toggleVisibleMoneyAll = () => {
        if (this.state.isVisibleAllMoney) this.setState({visibleMoneyList: []});

        this.setState({isVisibleAllMoney: !this.state.isVisibleAllMoney})
    };

    handleSearchClick = () => {
        let {from, to} = this.state;

        if (!from) {
            // this.setState({notification: 'Please, select date from'});
            return;
        } else if (!to) {
            // this.setState({notification: 'Please, select date to'});
            return;
        } else if (from > to) {
            // this.setState({notification: 'Please, check days range'});
            return
        }

        const {selectedDatesSubmitted} = this.state;
        const currentClicked = this.getCurrentSelectedDates(from, to);
        if (selectedDatesSubmitted !== currentClicked) {
            this.setState(prevState => ({selectedDatesSubmitted: currentClicked}))
        }
    };

    getCurrentSelectedDates = (from, to) => {
        return from.getTime() + '_' + to.getTime();
    };

    getBriefReport = () => {
        const {selectedDatesSubmitted, from, to} = this.state;
        if (!from || !to) return null;

        const skipNotSubmittedDates = this.getCurrentSelectedDates(from, to) !== selectedDatesSubmitted;
        if (skipNotSubmittedDates) return null;

        return (
            <Query query={GET_BRIEF_REPORT_QUERY}
                   variables={{
                       from: from.getTime() / 1000,
                       to: to.getTime() / 1000,
                   }}>
                {({loading, errors, data}) => {
                    if (loading) return 'loading...';
                    const projects = data && data.reportBriefProjects ? data.reportBriefProjects : [];

                    const {isVisibleAllMoney, visibleMoneyList} = this.state;

                    const list = projects.map(({id, name, spent}) => {
                        const isBlurred = isVisibleAllMoney || visibleMoneyList.indexOf(+id) !== -1;
                        return (
                            <li key={id}>
                                <span className='project-summary__name'>{name}</span>
                                <div className='project-summary__money'>
                                    <FontAwesomeIcon className='project-summary__money--symbol' icon={faHryvnia}/>
                                    <BlurredText id={id}
                                                 isBlurred={isBlurred}
                                                 onToggleBlur={this.toggleVisibleMoney}>{spent}
                                    </BlurredText>
                                </div>
                            </li>
                        )
                    });
                    return (
                        <div className='project-summary__report'>
                            <ul className='project-summary__items'>{list}</ul>
                        </div>
                    )
                }}
            </Query>
        )
    };

    render() {
        const {from, to} = this.state;
        const Report = this.getBriefReport;

        return (
            <div className='project-summary--wrapper'>
                <DualDaysPicker from={from}
                                to={to}
                                classNames='project-summary__days--wrapper'
                                onDayChangeFrom={this.handleDayFromChange}
                                onDayChangeTo={this.handleDayToChange}
                                btnName='Search'
                                onBtnClick={this.handleSearchClick}/>
                <Checkbox classNames='project-summary__checkbox'
                          id='visibleMoneyChbx'
                          isChecked={this.state.isVisibleAllMoney}
                          onCheckBoxChange={this.toggleVisibleMoneyAll}
                          label='Show all'/>
                <Report/>
            </div>
        )
    }
}

export default ReportBriefPage;