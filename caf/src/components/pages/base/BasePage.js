import React from 'react';

import './BasePage.scss';
import Header from "../../Header/Header";

const BasePage = props => {

   return (
       <div className="base-page--wrapper">
        <Header/>
        <section className='base-page--content'>
            {props.children}
        </section>
    </div>
   )
};

export default BasePage;