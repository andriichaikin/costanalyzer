import React from 'react';
import {withRouter} from "react-router-dom";

import {graphql} from 'react-apollo';
import {GET_ALL_PROJECTS_QUERY} from "../../graphql/query";

import TableContainer from "../Table/TableContainer";

const PROJECT_NAME = 'Project';
const HEADERS = [
    {name: PROJECT_NAME, field: 'name'},
    {name: 'Budget', field: 'budget'},
    {name: 'Spent', field: 'spent'},
    {name: 'Saldo', field: 'saldo'},
    {name: 'Status', field: 'status'},
    {name: 'KPI', field: 'kpi'},
    {name: 'Deadline', field: 'deadline'},
    {name: 'Comment', field: 'comment'},
];

const MainPage = ({data, loading, error, history}) => {

    const handleClick = (coordinates) => {
        const [x, y] = coordinates;
        if (x === 0) {
            console.log('x clicked')
        } else if (y === 0) {
            console.log('y clicked')
        } else {
            const {name} = HEADERS[y - 1];
            if (PROJECT_NAME === name && data && data.projects) {
                const project = data.projects[x - 1];
                history.push(`/report/detailed/${project.id}`)
            }
        }
    };

    return (
        <TableContainer headers={HEADERS}
                        rows={data.projects || []}
                        numerable={true}
                        onClick={handleClick}/>
    )
};

export default withRouter(graphql(GET_ALL_PROJECTS_QUERY)(MainPage))  //todo compose