import React, {Component} from 'react';
import {SubmissionError} from "redux-form";

import {withApollo} from "@apollo/react-hoc";

import './CreatePage.scss';

import Bookmarks, {BOOKMARKS} from "../common/Bookmarks";
import ProjectForm, {PROJECT_FORM_MODE} from "../common/form/ProjectForm";
import EmployeeForm, {EMPLOYEE_FORM_MODE} from "../common/form/EmployeeForm";

import {CREATE_EMPLOYEE_MUTATION, CREATE_PROJECT_MUTATION} from "../../graphql/mutation";
import {GET_ALL_PROJECTS_QUERY} from "../../graphql/query";

class CreatePage extends Component {

    state = {
        mode: BOOKMARKS.project,
    };

    handleProjectSubmit = values => {
        const variables = {...values, deadline: values.deadline.toString()};

        let client = this.props.client;
        return client.mutate({
            mutation: CREATE_PROJECT_MUTATION,
            variables,
            update: (proxy, {data}) => {
                try {
                    const cachedProjects = proxy.readQuery({query: GET_ALL_PROJECTS_QUERY});
                    cachedProjects.projects.push(data.createProject);
                    proxy.writeQuery({
                        query: GET_ALL_PROJECTS_QUERY,
                        data: {'projects': cachedProjects.projects}
                    })
                } catch (e) {
                    //main page not visited
                }
            }
        })
            .catch(error => {
                const graphQLError = error.graphQLErrors && error.graphQLErrors[0];
                if (graphQLError) {
                    if (graphQLError.reason) throw new SubmissionError(graphQLError.reason);  //server validation error
                    else console.error(graphQLError)
                }
                console.error(error);
            });
    };

    handleEmployeeSubmit = values => {
        let client = this.props.client;
        return client.mutate({
            mutation: CREATE_EMPLOYEE_MUTATION,
            variables: {...values}
        })
            .catch(error => {
                const graphQLError = error.graphQLErrors && error.graphQLErrors[0];
                if (graphQLError) {
                    if (graphQLError.reason) throw new SubmissionError(graphQLError.reason);  //server validation error
                    else console.error(graphQLError)
                }
                console.error(error);
            });
    };

    toggleMode = mode => this.setState({mode});

    getForm = () => {
        const {mode} = this.state;
        let isProjectMode = mode === BOOKMARKS.project;

        return (
            <div className='create-form--wrapper'>
                <div className='create-form'>
                    {isProjectMode ?
                        <ProjectForm form='projectCreate'
                                     mode={PROJECT_FORM_MODE.create}
                                     onSubmit={this.handleProjectSubmit}
                                     btnName='Save'/> :
                        <EmployeeForm form='employeeCreate'
                                      mode={EMPLOYEE_FORM_MODE.create}
                                      onSubmit={this.handleEmployeeSubmit}/>}
                </div>
            </div>
        )
    };

    render() {
        const {mode} = this.state;
        const Form = this.getForm;

        return (
            <div className='create-page'>
                <Bookmarks mode={mode} changeBookmark={this.toggleMode}/>
                <Form/>
            </div>
        )
    }
}

export default withApollo(CreatePage);