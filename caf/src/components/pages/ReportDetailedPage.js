import React, {Component} from 'react';

import {Query} from "react-apollo";
import {withApollo} from "@apollo/react-hoc";
import {
    GET_PROJECT_NAME_QUERY,
    GET_DETAILED_REPORT_QUERY,
    GET_DETAILED_REPORT_EMPLOYEE_KPI_QUERY
} from "../../graphql/query";

import 'react-day-picker/lib/style.css';
import './ReportDetailedPage.scss';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowAltCircleDown, faHryvnia} from "@fortawesome/free-solid-svg-icons";

import Checkbox from "../common/Checkbox";
import Spinner from "../common/Spinner";
import DualDaysPicker from "../common/DualDaysPicker";
import CirclePercentage from "../common/CirclePercentage";
import BlurredText from "../common/BlurredText";

class ReportDetailedPage extends Component {

    state = {
        from: null,
        to: null,
        selectedDatesSubmitted: '',
        visibleSalariesAll: false,
        visibleSalaries: [],
        visibleDescriptions: [],
    };

    handleDayFromChange = (selectedDay, modifiers, dayPickerInput) => {
        dayPickerInput.getInput().focus();
        this.setState({from: selectedDay})
    };

    handleDayToChange = (selectedDay, modifiers, dayPickerInput) => {
        dayPickerInput.getInput().focus();
        this.setState({to: selectedDay})
    };

    handleSearchClick = () => {
        let {from, to} = this.state;

        if (!from) {
            // this.setState({notification: 'Please, select date from'});
            return;
        } else if (!to) {
            // this.setState({notification: 'Please, select date to'});
            return;
        } else if (from > to) {
            // this.setState({notification: 'Please, check days range'});
            return
        }

        const {selectedDatesSubmitted} = this.state;
        const currentClicked = this.getCurrentSelectedDates(from, to);
        if (selectedDatesSubmitted !== currentClicked) {
            this.setState(prevState => ({selectedDatesSubmitted: currentClicked}))
        }
    };

    getCurrentSelectedDates = (from, to) => {
        return from.getTime() + '_' + to.getTime();
    };

    toggleSalaryVisible = id => {
        const employeeId = +id;

        const showed = [...this.state.visibleSalaries];
        if (showed.includes(employeeId)) {
            const indx = showed.indexOf(employeeId);
            showed.splice(indx, 1);
        } else {
            showed.push(employeeId);
        }

        this.setState({visibleSalaries: showed});
    };

    toggleVisibleSalariesAll = () => {
        if (this.state.visibleSalariesAll) this.setState({visibleSalaries: []});

        this.setState({visibleSalariesAll: !this.state.visibleSalariesAll})
    };

    toggleKPIDetails = (employeeId) => {
        const {visibleDescriptions} = this.state;
        const indx = visibleDescriptions.findIndex(id => id === employeeId);
        const copy = [...visibleDescriptions];
        if (indx === -1) {
            copy.push(employeeId);
        } else {
            copy.splice(indx, 1);
        }
        this.setState({visibleDescriptions: copy});
    };

    getEmployeesList = () => {
        const {selectedDatesSubmitted, from, to} = this.state;
        if (!from || !to) return null;

        const skipNotSubmittedDates = this.getCurrentSelectedDates(from, to) !== selectedDatesSubmitted;
        if (skipNotSubmittedDates) return null;

        const {id: projectId} = this.props.match.params;
        return (
            <div className='employee-cards--wrapper'>
                <Checkbox classNames='employee-cards__checkbox'
                          id='visibleSalariesChbx'
                          isChecked={this.state.visibleSalariesAll}
                          onCheckBoxChange={this.toggleVisibleSalariesAll}
                          label='Show all'/>

                <Query query={GET_DETAILED_REPORT_QUERY}
                       variables={{
                           projectId: +projectId,
                           from: from.getTime() / 1000,
                           to: to.getTime() / 1000,
                       }}>
                    {({loading, error, data = {}}) => {
                        if (loading) return 'loading..'

                        const {visibleSalaries, visibleSalariesAll, visibleDescriptions} = this.state;

                        const {reportDetailedProject: report} = data;
                        const employees = report && report.employees ? report.employees : [];

                        const list = [];
                        employees.forEach(employee => {
                            const {employeeId, name, surname, earnedMoney, hours} = employee;
                            const showDescription = visibleDescriptions.includes(employeeId);

                            const isSalaryVisible = visibleSalariesAll || visibleSalaries.indexOf(employeeId) !== -1;
                            const iconRotateUp = showDescription ? 'employee-card__icon--rotate' : '';
                            list.push(
                                <li key={employeeId}>
                                    <div className='employee-card'>
                                        <div className='employee-card__info'>
                                            <span className='employee-card__name'>{surname} {name}</span>
                                            <span className='employee-card__hours'>hours: {hours}</span>
                                            <FontAwesomeIcon icon={faHryvnia}/>
                                            <BlurredText id={employeeId}
                                                         classNames='employee-card__salary--wrapper'
                                                         isBlurred={isSalaryVisible}
                                                         onToggleBlur={this.toggleSalaryVisible}>
                                                {earnedMoney}
                                            </BlurredText>
                                            <FontAwesomeIcon icon={faArrowAltCircleDown}
                                                             className={`employee-card__icon ${iconRotateUp} fa-lg`}
                                                             onClick={this.toggleKPIDetails.bind(null, employeeId)}/>
                                        </div>
                                        {showDescription &&
                                        <Query query={GET_DETAILED_REPORT_EMPLOYEE_KPI_QUERY}
                                               variables={{
                                                   projectId: +projectId,
                                                   employeeId: +employeeId,
                                                   from: from.getTime() / 1000,
                                                   to: to.getTime() / 1000,
                                               }}>
                                            {({loading, error, data}) => {
                                                const reportEmployee = data && data.reportDetailedProjectEmployee;
                                                const weekdays = reportEmployee && reportEmployee.weekdays ?
                                                    reportEmployee.weekdays : {};
                                                const holidays = reportEmployee && reportEmployee.holidays ?
                                                    reportEmployee.holidays : {};

                                                return (
                                                    <div className='employee-card__kpi'>
                                                        <div className='employee-card__kpi-item'>
                                                            Total: {loading ?
                                                            <Spinner classNames='employee-card__kpi--spinner'/> :
                                                            <CirclePercentage classnames='employee-card__circle'
                                                                              colorStyle={{stroke: 'lightgreen'}}
                                                                              text={`${weekdays.hours}/${holidays.count + weekdays.count}`}
                                                                              percent={(weekdays.hours / (2 * 8)) * 100}/>
                                                        }
                                                        </div>
                                                        <div className='employee-card__kpi-item'>
                                                            Weekdays: {loading ?
                                                            <Spinner classNames='employee-card__kpi--spinner'/> :
                                                            <CirclePercentage classnames='employee-card__circle'
                                                                              colorStyle={{stroke: 'lightgreen'}}
                                                                              text={`${weekdays.hours}/${weekdays.count}`}
                                                                              percent={(weekdays.hours / (weekdays.count * 8)) * 100}/>}
                                                        </div>
                                                        <div className='employee-card__kpi-item'>
                                                            Holidays: {loading ?
                                                            <Spinner classNames='employee-card__kpi--spinner'/> :
                                                            <CirclePercentage classnames='employee-card__circle'
                                                                              colorStyle={{stroke: 'lightgreen'}}
                                                                              text={`${holidays.hours}/${holidays.count}`}
                                                                              percent={(holidays.hours / (holidays.count * 8)) * 100}/>}
                                                        </div>
                                                    </div>
                                                )
                                            }}
                                        </Query>}
                                    </div>
                                </li>
                            );
                        });

                        return <ul>{list}</ul>
                    }}
                </Query>
            </div>
        );
    };

    render() {
        const {from, to} = this.state;
        const InvolvedEmployees = this.getEmployeesList;

        let urlId = this.props.match.params.id;
        return (
            <Query query={GET_PROJECT_NAME_QUERY} variables={{prId: +urlId}}>
                {({loading, error, data}) => {
                    if (loading) return "loading...";
                    if (error) return `Error! ${error.message}`;
                    const {project} = data;

                    return (
                        <div className='project-page--wrapper'>
                             <span className='project-page__caption'>
                                Project name:<span className='project-page__name'>{project && project.name}</span>
                             </span>
                            <DualDaysPicker from={from}
                                            to={to}
                                            classNames='project-page__days--wrapper'
                                            onDayChangeFrom={this.handleDayFromChange}
                                            onDayChangeTo={this.handleDayToChange}
                                            btnName='Search'
                                            onBtnClick={this.handleSearchClick}/>
                            <InvolvedEmployees/>
                        </div>
                    )
                }}
            </Query>
        )
    }
}

export default withApollo(ReportDetailedPage);