import React, {Component} from 'react';
import Select from "react-select";

import './EditPage.scss';

import {connect} from "react-redux";
import {SubmissionError} from "redux-form";

import ProjectForm, {PROJECT_FORM_MODE} from "../common/form/ProjectForm";
import EmployeeForm, {EMPLOYEE_FORM_MODE} from "../common/form/EmployeeForm";
import Bookmarks, {BOOKMARKS} from "../common/Bookmarks";

import API from "../../api/API";
import {Query} from 'react-apollo';

import {
    GET_ALL_EMPLOYEES_NAMES_QUERY,
    GET_ALL_PROJECTS_NAMES_QUERY,
    GET_EMPLOYEE_BY_ID_QUERY,
    GET_PROJECT_BY_ID_QUERY
} from "../../graphql/query";
import {UPDATE_EMPLOYEE_MUTATION, UPDATE_PROJECT_MUTATION} from "../../graphql/mutation";
import {withApollo} from "@apollo/react-hoc";

class EditPage extends Component {

    state = {
        mode: BOOKMARKS.project,
        selected: {},
        defaultValues: {
            project: {
                id: '',
                name: '',
                budget: '',
                spent: '',
                saldo: '',
                status: {id: '', label: ''},
                kpi: '',
                deadline: '',
                comment: ''
            },
            employee: {
                id: '', name: '', surname: '', salary: '', position: {id: '', label: ''},
            },
        },
    };

    toggleMode = mode => {
        this.setState({mode, selected: {}});
    };

    handleSelectChange = ({label, value}) => this.setState({selected: {label, value}});

    handleProjectSubmit = values => {

        const {client} = this.props;
        return client.mutate({
            mutation: UPDATE_PROJECT_MUTATION,
            variables: {...values},
        })
            .catch(error => {
                const graphQLError = error.graphQLErrors && error.graphQLErrors[0];
                if (graphQLError) {
                    if (graphQLError.reason) throw new SubmissionError(graphQLError.reason);  //server validation error
                    else console.error(graphQLError)
                }
                console.error(error);
            });
    };

    handleEmployeeSubmit = values => {
        const {client} = this.props;

        return client.mutate({
            mutation: UPDATE_EMPLOYEE_MUTATION,
            variables: {...values}
        })
            .then(res => console.log('ok'))
            .catch(error => {
                const graphQLError = error.graphQLErrors && error.graphQLErrors[0];
                if (graphQLError) {
                    if (graphQLError.reason) throw new SubmissionError(graphQLError.reason);  //server validation error
                    else console.error(graphQLError)
                }
                console.error(error);
            });
    };

    generateOptions = arr => {
        const {mode} = this.state;
        let opts = [];
        if (BOOKMARKS.project === mode) {
            opts = arr.map(project => ({value: project.id, label: project.name}));
        } else {
            opts = arr.map(employee => {
                const {name, surname} = employee;
                const text = surname.charAt(0).toUpperCase() + surname.slice(1) + ' ' + name.charAt(0).toUpperCase() + '.';
                return {value: employee.id, label: text}
            });
        }

        const {selected} = this.state;
        return (
            <div className='edit-page__options'>
                <Select value={selected} options={opts} onChange={this.handleSelectChange}/>
            </div>
        )
    };

    getForm = () => {
        const {mode} = this.state;

        const isProjectMode = mode === BOOKMARKS.project;
        const getByIdQuery = isProjectMode ? GET_PROJECT_BY_ID_QUERY : GET_EMPLOYEE_BY_ID_QUERY;
        const {value} = this.state.selected;
        return (
            <Query query={getByIdQuery} variables={{id: +value}} skip={value === undefined || value === null}>
                {({loading, errors, data}) => {
                    // if (loading) return 'Loading...';

                    let obj = undefined;
                    if (data) {
                        obj = data && isProjectMode ? {
                            ...data.project,
                            deadline: new Date(+data.project.deadline),
                            status: {id: data.project.status, label: data.project.status}
                        } : {
                            ...data.employee,
                            position: {id: data.employee.id, label: data.employee.position}
                        };
                    }

                    return (
                        <div className='edit-form--wrapper'>
                            <div className='edit-form'>
                                {isProjectMode ?
                                    <ProjectForm form='projectEdit'
                                                 initialValues={obj || this.state.defaultValues.project}
                                                 date={obj && obj.deadline}
                                                 mode={PROJECT_FORM_MODE.edit}
                                                 onSubmit={this.handleProjectSubmit}
                                                 btnName='Update'/> :
                                    <EmployeeForm form='employeeEdit'
                                                  initialValues={obj || this.state.defaultValues.employee}
                                                  mode={EMPLOYEE_FORM_MODE.edit}
                                                  onSubmit={this.handleEmployeeSubmit}/>
                                }
                            </div>
                        </div>
                    )
                }}
            </Query>
        )
    };

    render() {
        const {mode} = this.state;

        let isProjectMode = mode === BOOKMARKS.project;
        const namesQuery = isProjectMode ? GET_ALL_PROJECTS_NAMES_QUERY : GET_ALL_EMPLOYEES_NAMES_QUERY;

        return (
            <Query query={namesQuery}>
                {({loading, errors, data}) => {
                    if (loading) return 'Loading...';

                    const arr = isProjectMode ? data.projects : data.employees;

                    const Options = this.generateOptions.bind(this, arr);
                    const Form = this.getForm;
                    return (
                        <div className='edit-page'>
                            <Bookmarks mode={mode} changeBookmark={this.toggleMode}/>

                            <div className='edit-page__content'>
                                <Options/>
                                <Form/>
                            </div>
                        </div>
                    )
                }}
            </Query>
        )
    }
}

export default withApollo(EditPage);