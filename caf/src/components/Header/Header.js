import React, {Component} from 'react';

import './Header.scss';
import {Link, NavLink} from "react-router-dom";

class Header extends Component {

    state = {
        isMenuOpened: true,
    };

    toggleMenu = () => {
        // this.setState({isMenuOpened: true})
        this.setState({isMenuOpened: !this.state.isMenuOpened})
    };

    handleSubMenuClick = () => {
        // this.setState({isMenuOpened: true})
        this.setState({isMenuOpened: false})
    };

    render() {
        const {isMenuOpened} = this.state;

        const btnOpenedStyles = isMenuOpened ? ' fold-out__line--opened' : ' fold-out__line--closed';
        const subMenuOpenedStyles = isMenuOpened ? ' sub-menu--opened' : ' sub-menu--closed';

        return (
            <header className='header'>
                <nav className='nav'>
                    <NavLink to='/'
                             exact={true}
                             title='Main'
                             activeClassName='nav__active'>Main</NavLink>
                    <NavLink to='/report/brief'
                             title='Brief'
                             activeClassName='nav__active'>Brief</NavLink>
                    <NavLink to='/forecast'
                             title='Forecast'
                             activeClassName='nav__active'>Forecast</NavLink>
                    <NavLink to='/chronology'
                             title='Report'
                             activeClassName='nav__active'>Chronology</NavLink>
                </nav>
                <div className='fold-out--wrapper'>
                    <div className='fold-out__menu' onClick={this.toggleMenu}>
                        <span className={`fold-out__line${btnOpenedStyles}`}></span>
                        <span className={`fold-out__line${btnOpenedStyles}`}></span>
                        <span className={`fold-out__line${btnOpenedStyles}`}></span>
                    </div>
                </div>
                <div className={`sub-menu${subMenuOpenedStyles}`}>
                    <ul>
                        <Link to='/create' title='Create' onClick={this.handleSubMenuClick}>Create</Link>
                        <Link to='/edit' title='Edit' onClick={this.handleSubMenuClick}>Edit</Link>
                        <Link to='/account' title='Account' onClick={this.handleSubMenuClick}>Account</Link>
                    </ul>
                </div>
            </header>
        )
    };
}

export default Header;