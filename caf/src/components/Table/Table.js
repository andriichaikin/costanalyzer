import React from 'react';
import Row from "./Row";
import HeaderRow from './HeaderRow';

const Table = ({
                   headers, numerable,
                   selectedRow, selectRow,
                   selectedColumn, selectColumn,
                   onClick, ...props
               }) => {

    const Rows = () => {
        return props.rows
            .map((obj, xAxis) => {
                return <Row key={xAxis}
                            headers={headers}
                            row={obj}
                            xAxis={xAxis + 1}
                            numerable={numerable}
                            isSelected={xAxis === selectedRow}
                            selectRow={selectRow}
                            selectedColumn={selectedColumn}
                            onClick={onClick}/>
            });
    };

    return (
        <div className='table-wrapper'>
            <table>
                <thead>
                <HeaderRow headers={headers}
                           numerable={numerable}
                           selectedColumn={selectedColumn}
                           selectColumn={selectColumn}
                           onClick={onClick}/>
                </thead>
                <tbody>
                <Rows/>
                </tbody>
            </table>
        </div>
    )
};

export default Table;