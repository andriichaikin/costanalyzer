import React from 'react';
import HeaderCell from "./HeaderCell";

const HeaderRow = ({headers, numerable, onClick, selectColumn}) => {

    const result = [];
    if (numerable) result.push(<HeaderCell key='n/n'
                                           value='n/n'
                                           coordinates={[0, 0]}
                                           onClick={onClick}/>);

    headers.forEach((header, yAxis) => {
        return result.push(<HeaderCell key={header.name}
                                       value={header.name}
                                       coordinates={[0, yAxis + 1]}
                                       onClick={onClick}
                                       onDblClick={selectColumn}/>)
    });

    return <tr>{result}</tr>;
};

export default HeaderRow;