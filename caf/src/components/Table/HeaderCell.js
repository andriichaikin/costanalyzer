import React from 'react';

const HeaderCell = ({coordinates, onClick, onDblClick, ...props}) => {

    const handleClick = () => {
        if (onClick) onClick(coordinates);
    };

    const handleDblClick = () => {
        if (onDblClick) onDblClick(coordinates);
    };

    return <th onClick={handleClick}
               onDoubleClick={handleDblClick}>{props.value}</th>
};

export default HeaderCell;