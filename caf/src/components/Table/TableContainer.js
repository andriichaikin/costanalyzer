import React, {Component} from 'react';

import Table from './Table';
import './Table.scss';

class TableContainer extends Component {

    state = {
        selectedRow: undefined,
        selectedColumn: undefined,
    };

    selectRow = coordinates => {
        const xAxis = coordinates[0];
        if (this.state.selectedRow !== xAxis) this.setState({selectedRow: xAxis});
        else this.setState({selectedRow: null});
    };

    selectColumn = coordinates => {
        const yAxis = coordinates[1];
        if (this.state.selectedColumn !== yAxis) this.setState({selectedColumn: yAxis});
        else this.setState({selectedColumn: null});
    };

    render() {
        const {headers, rows, numerable, onClick} = this.props;
        const {selectedRow, selectRow, selectedColumn, selectColumn} = this.state;

        return <Table headers={headers}
                      rows={rows}
                      selectedRow={selectedRow}
                      selectRow={selectRow}
                      selectedColumn={selectedColumn}
                      selectColumn={selectColumn}
                      numerable={numerable}
                      onClick={onClick}/>
    }
}

export default TableContainer;