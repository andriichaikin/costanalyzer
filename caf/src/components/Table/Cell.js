import React, {Component} from 'react';

class Cell extends Component {

    state = {
        styles: {
            whiteSpace: 'noWrap'
        },
    };

    handleClick = () => {
        const {coordinates, onClick} = this.props;
        if (onClick) onClick(coordinates);
    };

    handleDblClick = () => {
        this.toggleWhiteSpace();
        // if (this.props.onDblClick) this.props.onDblClick(this.props.coordinates);
    };

    toggleWhiteSpace = () => {
        if (this.state.styles.whiteSpace === 'noWrap') {
            this.setState({
                styles: {
                    ...this.state.styles,
                    whiteSpace: 'normal',
                }
            });
        } else {
            this.setState({
                styles: {
                    ...this.state.styles,
                    whiteSpace: 'noWrap',
                }
            });
        }
    };

    render() {
        const outterStyles = this.props.outterStyles || '';

        return <td data-label={this.props.dataLabel}
                   style={this.state.styles}
                   className={outterStyles}
                   onClick={this.handleClick}
                   onDoubleClick={this.handleDblClick}>{this.props.value}</td>
    }
}

export default Cell;