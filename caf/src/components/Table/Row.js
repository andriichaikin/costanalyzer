import React from 'react';
import Cell from "./Cell";

import './Row.scss';
import {withRouter} from "react-router-dom";

const Row = ({headers, xAxis, isSelected, numerable, onClick, ...props}) => {

    const Cells = () => {
        let rowHighlighted = '';
        if (isSelected) rowHighlighted = ' row--highlighted';

        const row = [];
        if (numerable)
            row.push(<Cell key='iter'
                           coordinates={[xAxis, 0]}
                           value={xAxis}
                           outterStyles={`${rowHighlighted} numeration`}
                           onClick={onClick}
                           onDblClick={props.selectRow}/>);

        headers.forEach((header, yAxis) => {
            let columnHighlighted = '';
            if (props.selectedColumn === yAxis) columnHighlighted = ' column--highlighted ';

            const value = props.row[header.field] || '';
                row.push(<Cell key={yAxis}
                               dataLabel={header.field}
                               coordinates={[xAxis, yAxis + 1]}
                               outterStyles={`${rowHighlighted}${columnHighlighted} cell__link`}
                               value={value}
                               onClick={onClick}/>);
        });

        return row;
    };

    return <tr className='row--wrapper'><Cells/></tr>
};

export default withRouter(Row);