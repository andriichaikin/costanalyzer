import React from 'react';
import './App.scss';
import Routes from "./Routes";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store";

import {ApolloProvider} from 'react-apollo';
import apolloClient from './graphql/apollo';

function App() {
    return (
        <ApolloProvider client={apolloClient}>
            <Provider store={store}>
                <BrowserRouter>
                    <Routes/>
                </BrowserRouter>
            </Provider>
        </ApolloProvider>
    );
}

export default App;