import {
    PROJECT_EDIT_SET_PROJECT,
    PROJECT_EDIT_SET_EMPLOYEE,
    PROJECT_EDIT_SET_PROJECT_NAMES,
    PROJECT_EDIT_SET_EMPLOYEE_NAMES,
    PROJECT_EDIT_RESET_FORM_VALUES,
} from '../constants';

import API from "../../api/API";

export const getProjectNames = () => dispatch => {
    API.get('/project/all/name')
        .then(res => res.data)
        .then(json => dispatch(__setProjectNames(json)))
        .catch(err => console.error(err));
};

const __setProjectNames = projectNames => {
    return {
        type: PROJECT_EDIT_SET_PROJECT_NAMES,
        projectNames,
    }
};

export const getEmployeeNames = () => dispatch => {
    API.get('/employee/all/name')
        .then(res => res.data)
        .then(json => dispatch(__setEmployeeNames(json)))
        .catch(err => console.error(err));
};

const __setEmployeeNames = employeeNames => {
    return {
        type: PROJECT_EDIT_SET_EMPLOYEE_NAMES,
        employeeNames,
    }
};

export const getProjectInitialValues = id => dispatch => {
    API.get(`/project/${id}`)
        .then(res => res.data)
        .then(project => {
            if (project && project.status) {
                const status = {value: project.status, label: project.status};
                return {...project, status};
            }
            return project;
        })
        .then(json => dispatch(__setProjectInfo(json)))
        .catch(err => console.error(err));
};

const __setProjectInfo = projectInfo => {
    return {
        type: PROJECT_EDIT_SET_PROJECT,
        projectInfo,
    }
};

export const getEmployeeInitialValues = id => dispatch => {
    API.get(`/employee/${id}`)
        .then(res => res.data)
        .then(employee => {
            if(employee && employee.position) {
                const position = {value: employee.position, label: employee.position};
                return {...employee, position};
            }
            return employee;
        })
        .then(json => dispatch(__setEmployeeInfo(json)))
        .catch(err => console.error(err));
};

const __setEmployeeInfo = employeeInfo => {
    return {
        type: PROJECT_EDIT_SET_EMPLOYEE,
        employeeInfo,
    }
};

export const resetFormValues = () => {
    return {type: PROJECT_EDIT_RESET_FORM_VALUES}
};