import API from '../../api/API';

import {
    PROJECT_STATISTIC__SET_EMPLOYEES_BY_DATE,
    PROJECT_STATISTIC__SET_EMPLOYEE_KPI,
    PROJECT_STATISTIC__SET_PROJECT_NAME,
} from '../constants';

export const getDaysByProjectId = (id, from = '1577664000000', to = '1579182400000') => dispatch => {

    API.get(`/report/detailed?projectId=1&from=1580515200&to=1583020800`)
    // API.get(`/report/detailed?projectId=${id}&from=${from}&to=${to}`)
        .then(res => res.data)
        .then(json => dispatch(__setDaysByProjectId(id, json)))
        .catch(err => console.log('Error detailed report:', err));
};

export const getEmployeeKPI = (employeeId, projectId, from, to) => dispatch => {
    console.log('getEmployeeKPI')
    API.get('/report/detailed/kpi?employeeId=1&projectId=1&from=1580515200&to=1583020800')
        .then(res => res.data)
        .then(data => {
            console.log(JSON.stringify(data))
            return data;
        })
        .then(json => dispatch(__setEmployeeKPI(employeeId, json)))
        .catch(err => console.error('Error getEmployeesKPI:', err));
};

export const getProjectName = id => dispatch => {
    API.get(`/project/${id}/name`)
        .then(res => res.data)
        .then(name => dispatch(__setProjectName(name, id)))
        .catch(err => console.error('getProjectNameError:', err));
};

const __setDaysByProjectId = (id, employees) => {
    return {
        type: PROJECT_STATISTIC__SET_EMPLOYEES_BY_DATE,
        employees,
    }
};

const __setEmployeeKPI = (employeeId, kpi) => {
    return {
        type: PROJECT_STATISTIC__SET_EMPLOYEE_KPI,
        kpi,
    };
};

const __setProjectName = (id, name) => {
    return {
        type: PROJECT_STATISTIC__SET_PROJECT_NAME,
        id,
        name,
    }
};