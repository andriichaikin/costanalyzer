import API from "../../api/API";

import {MAIN_PAGE__SET_ALL_PROJECTS} from '../constants';

export const getAllProject = () => dispatch => {

    API.get('/project/all')
        .then(res => res.data)
        .then(json => dispatch(__setAllProjects(json)))
        .catch(err => console.error(err));
};

const __setAllProjects = projects => {
    return {
        type: MAIN_PAGE__SET_ALL_PROJECTS,
        projects,
    }
};