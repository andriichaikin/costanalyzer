import {PROJECT_INFO_GENERAL__SET_STATISTIC} from '../constants';
import API from "../../api/API";

export const getAllProjectSummary = (from, to) => dispatch => {

    API.get('/report/general?from=1580515200&to=1583020800')
        .then(res => res.data)
        .then(json => dispatch(__setAllProjectSummary(json)))
        .catch(err => console.error('Catch exception in report summary request:', err));
};

const __setAllProjectSummary = projects => {
    return {
        type: PROJECT_INFO_GENERAL__SET_STATISTIC,
        projects,
    }
};