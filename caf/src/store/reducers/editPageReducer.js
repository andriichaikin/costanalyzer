import {
    PROJECT_EDIT_SET_PROJECT,
    PROJECT_EDIT_SET_PROJECT_NAMES,
    PROJECT_EDIT_SET_EMPLOYEE_NAMES,
    PROJECT_EDIT_RESET_FORM_VALUES, PROJECT_EDIT_SET_EMPLOYEE,
} from '../constants';

function editPageReducer(state = initialState, action) {

    switch (action.type) {
        case PROJECT_EDIT_SET_PROJECT: {
            return {
                ...state,
                projectInfo: action.projectInfo,
            }
        }
        case PROJECT_EDIT_SET_EMPLOYEE: {
            return {
                ...state,
                employeeInfo: action.employeeInfo,
            }
        }
        case PROJECT_EDIT_SET_PROJECT_NAMES: {
            return {
                ...state,
                projectNames: action.projectNames,
            }
        }
        case PROJECT_EDIT_SET_EMPLOYEE_NAMES: {
            return {
                ...state,
                employeeNames: action.employeeNames,
            }
        }
        case PROJECT_EDIT_RESET_FORM_VALUES: {
            return {
                ...state,
                projectInfo: {},
                employeeInfo: {},
                projectNames: [],
                employeeNames: [],
            }
        }
        default:
            return state;
    }
}

const initialState = {
    projectInfo: {},
    employeeInfo: {},
    projectNames: [],
    employeeNames: [],
};

export default editPageReducer;