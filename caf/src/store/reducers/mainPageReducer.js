import {MAIN_PAGE__SET_ALL_PROJECTS} from "../constants";

function mainPageReducer(state = initialState, action) {

    switch (action.type) {

        case MAIN_PAGE__SET_ALL_PROJECTS: {
            return {
                ...state,
                projects: action.projects
            }
        }
        default:
            return state;
    }
}

export default mainPageReducer;

const initialState = {
    projects: [],
        /* [
        {
            id: 11111,
            name: 'Project 1',
            budget: 100,
            spent: 20,
            saldo: 80808080808080,
            // balance: 80808080808080,
            // forecast_1: 111,
            // forecast_2: 222,
            status: 'OPEN',
            kpi: 1.2,
            deadline: '12-12-2012',
            // deadLine: '12-12-2012',
            comment: 'Description DescriptionDescription Description DescriptionDescription DescriptionDescription Description DescriptionDescription DescriptionDescription Description Description'
        },
        */
};