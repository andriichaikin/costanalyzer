import {combineReducers} from 'redux';
import projectStatisticPageReducer from './projectStatisticPageReducer';
import mainPageReducer from './mainPageReducer';
import projectInfoGeneralReducer from './projectInfoGeneralReducer';
import createPageReducer from "./createPageReducer";
import {reducer as formReducer} from 'redux-form';
import editPageReducer from "./editPageReducer";

export default combineReducers({
    mainPageReducer,
    projectStatisticPageReducer,
    projectInfoGeneralReducer,
    createPageReducer,
    editPageReducer,
    form: formReducer,
})