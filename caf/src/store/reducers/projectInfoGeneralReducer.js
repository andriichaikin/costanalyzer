import {PROJECT_INFO_GENERAL__SET_STATISTIC} from '../constants';

function projectInfoGeneralReducer(state = initialState, action) {

    switch (action.type) {

        case PROJECT_INFO_GENERAL__SET_STATISTIC: {
            return {
                ...state,
                projects: action.projects,
            }
        }
        default:
            return state;
    }
}

const initialState = {
    projects: [],
};

export default projectInfoGeneralReducer;
