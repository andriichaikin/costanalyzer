import {
    PROJECT_STATISTIC__SET_EMPLOYEES_BY_DATE,
    PROJECT_STATISTIC__SET_EMPLOYEE_KPI, PROJECT_STATISTIC__SET_PROJECT_NAME,
} from "../constants";

function projectStatisticPageReducer(state = defaultState, action) {

    switch (action.type) {
        case PROJECT_STATISTIC__SET_EMPLOYEES_BY_DATE: {
            return {
                ...state,
                project: {
                    ...state.project,
                    employees: action.employees,
                }
            }
        }
        case PROJECT_STATISTIC__SET_EMPLOYEE_KPI: {
            const {employees} = state.project;
            const {kpi} = action;

            const indx = employees.findIndex(empl => empl.employeeId === kpi.employeeId);
            if (indx === -1) return state;

            const toUpdate = [...employees];
            toUpdate[indx] = {
                ...toUpdate[indx],
                kpi: {
                    weekdays: {...kpi.weekdays},
                    holidays: {...kpi.holidays}
                }
            };

            return {
                ...state,
                project: {
                    ...state.project,
                    employees: toUpdate
                }
            };
        }
        case PROJECT_STATISTIC__SET_PROJECT_NAME: {
            return {
                ...state,
                project: {
                    ...state.project,
                    name: action.name,
                }
            }
        }
        default:
            return state;
    }
}

export default projectStatisticPageReducer;

const defaultState = {
    project: {
        id: null,
        name: '',
        employees: [],
    },
    projects: [11111, 11112, 11113, 11114, 11115],
};