import React, {Suspense, lazy} from 'react';
import {withRouter, Switch, Route, Redirect} from 'react-router-dom';
import BasePage from "./components/pages/base/BasePage";

const waitFor = Tag => props => <Tag {...props}/>;

const MainPage = lazy(() => import ('./components/pages/MainPage'));
const CreatePage = lazy(() => import('./components/pages/CreatePage'));
const EditPage = lazy(() => import('./components/pages/EditPage'));
const ReportDetailed = lazy(() => import('./components/pages/ReportDetailedPage'));
const ReportBrief = lazy(() => import('./components/pages/ReportBriefPage'));
const ForecastPage = lazy(() => import('./components/pages/ForecastPage'));
const ChronologyPage = lazy(() => import('./components/pages/ChronologyPage'));
const NotFoundPage = lazy(() => import('./components/pages/NotFoundPage'));
const AccountPage = lazy(() => import('./components/pages/AccountPage'));

const Routes = ({location}) => {

    return (
        <BasePage>
            <Suspense fallback={<div>pls wait...</div>}>
                <Switch location={location}>
                    <Route exact path='/' component={waitFor(MainPage)}/>
                    <Route path='/create' component={waitFor(CreatePage)}/>
                    <Route path='/edit' component={waitFor(EditPage)}/>
                    <Route path='/report/brief' component={waitFor(ReportBrief)}/>
                    <Route exact path='/report/detailed/:id' component={waitFor(ReportDetailed)}/>

                    <Route path='/forecast' component={waitFor(ForecastPage)}/>
                    <Route path='/chronology' component={waitFor(ChronologyPage)}/>

                    <Route path='/account' component={waitFor(AccountPage)}/>

                    <Route path='/not-found' component={waitFor(NotFoundPage)}/>
                    <Redirect to='/not-found'/>
                </Switch>
            </Suspense>
        </BasePage>
    )
};

export default withRouter(Routes);