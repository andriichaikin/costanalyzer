CREATE TABLE IF NOT EXISTS system_user
(
    id       serial,
    name     varchar(64) NOT NULL,
    surname varchar(64),
    email    varchar(64) NOT NULL UNIQUE,
    password varchar(64) NOT NULL,
    phone varchar(64),
);
ALTER TABLE system_user
    ADD CONSTRAINT pk_system_user_id PRIMARY KEY (id);
CREATE UNIQUE INDEX ak_system_user_id ON system_user (id);

CREATE TABLE IF NOT EXISTS role
(
    id   serial,
    role varchar(64) NOT NULL UNIQUE
);
ALTER TABLE role
    ADD CONSTRAINT pk_system_user_role_id PRIMARY KEY (id);
CREATE UNIQUE INDEX ak_system_user_role_id ON role (id);

CREATE TABLE IF NOT EXISTS system_user_role
(
    id             serial,
    system_user_id integer NOT NULL,
    role_id        integer NOT NULL
);
ALTER TABLE system_user_role
    ADD CONSTRAINT pk_system_user_role_id PRIMARY KEY (id);
ALTER TABLE system_user_role
    ADD CONSTRAINT fk_system_user_role_system_user_id FOREIGN KEY (system_user_id) REFERENCES system_user (id) ON DELETE RESTRICT;
ALTER TABLE system_user_role
    ADD CONSTRAINT fk_system_user_role_role_id FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE RESTRICT;


CREATE TABLE IF NOT EXISTS employee
(
    id       serial,
    name     varchar(64) NOT NULL,
    surname  varchar(64) NOT NULL,
    position varchar(64) NOT NULL,
    salary   INTEGER     NOT NULL
);
ALTER TABLE employee
    ADD CONSTRAINT pk_employee_id PRIMARY KEY (id);
CREATE UNIQUE INDEX ak_employee_id ON employee (id);


CREATE TABLE IF NOT EXISTS project
(
    id       serial,
    name     varchar(64) NOT NULL UNIQUE,
    budget   INTEGER     NOT NULL,
    spent    INTEGER     NOT NULL,
    saldo    INTEGER     NOT NULL,
    status   varchar(64) not null,
    kpi      INTEGER,
    deadline date        NOT NULL,
    comment  varchar(4096)
);
ALTER TABLE project
    ADD CONSTRAINT pk_project_id PRIMARY KEY (id);
CREATE UNIQUE INDEX ak_project_id ON project (id);


CREATE TABLE IF NOT EXISTS report
(
    id         serial,
    project_id integer NOT NULL
);
ALTER TABLE report
    ADD CONSTRAINT pk_report_id PRIMARY KEY (id);
CREATE UNIQUE INDEX ak_report_id ON report (id);
ALTER TABLE report
    ADD CONSTRAINT fk_project_id FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE RESTRICT;


-- CREATE TABLE IF NOT EXISTS forecast
-- (
--     id         serial,
--     project_id integer NOT NULL
-- );
-- ALTER TABLE forecast
--     ADD CONSTRAINT pk_forecast_id PRIMARY KEY (id);
-- ALTER TABLE forecast
--     ADD CONSTRAINT fk_project_id FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE RESTRICT;

CREATE TABLE IF NOT EXISTS day
(
    id         serial,
    date       date        NOT NULL UNIQUE,
    day        varchar(64) NOT NULL,
    is_holiday boolean
);
ALTER TABLE day
    ADD CONSTRAINT pk_day_id PRIMARY KEY (id);
CREATE UNIQUE INDEX ak_day_id ON day (id);


CREATE TABLE IF NOT EXISTS involved
(
    id           serial,
    earned_money integer NOT NULL,
    hours        integer NOT NULL,
    day_id       integer NOT NULL,
    employee_id  integer NOT NULL,
    report_id    integer NOT NULL
);
ALTER TABLE involved
    ADD CONSTRAINT pk_involved_id PRIMARY KEY (id);
CREATE UNIQUE INDEX ak_involved_id ON involved (id);
ALTER TABLE involved
    ADD CONSTRAINT fk_day_id FOREIGN KEY (day_id) REFERENCES day (id) ON DELETE RESTRICT;
ALTER TABLE involved
    ADD CONSTRAINT fk_report_id FOREIGN KEY (report_id) REFERENCES report (id) ON DELETE RESTRICT;
ALTER TABLE involved
    ADD CONSTRAINT fk_employee_id FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE RESTRICT;
ALTER TABLE involved
    ADD UNIQUE (day_id, employee_id, report_id)