const projects = require('./api/projects');
const reports = require('./api/reports');
const users = require('./api/users');
const employees = require('./api/employees');

module.exports = app => {
    app.use('/api/project', projects);
    app.use('/api/report', reports);
    app.use('/api/users', users);
    app.use('/api/employee', employees);

    app.use(function (req, res, next) {
        console.log(`incoming request for ${req.path} ...`);
        next();
    });
};