const router = require('express').Router();
const db = require('../../db');

// router.get('/all/name', (req, res) => {
//     db.query('SELECT id, name, surname FROM employee')
//         .then(result => result.rows)
//         .then(rows => res.json(rows))
//         .catch(err => res.status(500).json({message: err.message}));
// });
//
// router.post('/save', (req, res) => {  //todo bcryptjs
//     const {employee} = req.body;
//     db.query('INSERT INTO employee (name, surname, position, salary) VALUES ($1, $2, $3, $4) RETURNING id',
//         [employee.name, employee.surname, employee.position, employee.salary])
//         .then(result => result.rows)
//         .then(rows => rows[0])
//         .then(row => row.id)
//         .then(id => res.status(201).json({employeeId: id}))
//         .catch(err => {
//             console.error(err);
//             return res.status(500).send({message: err.message})
//         });
// });

// router.post('/update', (req, res) => {
//     const {employee} = req.body;
//     db.query('UPDATE employee ' +
//         'SET name = $1, surname = $2, position = $3, salary = $4 ' +
//         'WHERE id = $5',
//         [employee.name, employee.surname, employee.position, employee.salary, employee.id])
//         .then(() => res.status(201).send())
//         .catch(err => {
//             console.error(err);
//             return res.status(500).send({message: err.message})
//         });
// });

// router.get('/:id', (req, res) => {
//     db.query('SELECT DISTINCT * FROM employee WHERE id=$1', [req.params.id])
//         .then(result => result.rows)
//         .then(rows => rows[0])
//         .then(row => {
//             if (row) {
//                 res.status(200).json(row);
//             } else (
//                 res.status(204).json({message: `Employee with id ${req.params.id} not found`})
//             )
//         })
//         .catch(err => res.status(500).json({message: err.message}));
// });

module.exports = router;