const validateProject = require('../../services/projectService').validateProject;
const prepareProject = require('../../services/projectService').prepareProject;

const router = require('express').Router();
const db = require('../../db/index');

// router.get('/all', (req, res) => {
//     db.query('SELECT * FROM project')
//         .then(result => result.rows)
//         .then(rows => res.json(rows))
//         .catch(err => console.error(err));
// });
//
// router.get('/all/name', (req, res) => {
//     db.query('SELECT id, name FROM project')
//         .then(result => result.rows)
//         .then(rows => {
//             console.log(`found ${rows.length} rows`);
//             return rows;
//         })
//         .then(rows => res.json(rows))
//         .catch(err => console.error(err));
// });
//
// router.get('/find', (req, res) => {
//     const {query} = req;
//     const {name} = query;
//     let keyName = Object.keys(query).find(key => query[key] === name);
//
//     db.query('SELECT EXISTS(SELECT 1 FROM project WHERE name ILIKE $1)', [name])
//         .then(result => result.rows)
//         .then(rows => rows[0])
//         .then(row => row.exists)
//         .then(exists => {
//             console.log(`${keyName} with ${name} exist ===  ${exists}`)
//             if (exists) res.status(200).send({message: `Project with property ${keyName} exist`});
//             else res.status(204).send({message: `Project with property ${keyName} does not exist`})
//         })
//         .catch(err => console.error(err));
// });
//
// router.post('/save', (req, res) => {
//     const {project} = req.body;
//
//     const errors = validateProject(project);
//     console.log(errors);
//     if (Object.keys(errors).length) res.status(400).json(errors);
//
//     project.kpi = 0;
//
//     const prepared = prepareProject(project);
//
//     db.query('INSERT INTO project (name, budget, spent, saldo, status, kpi, deadline, comment) ' +
//         'VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id',
//         [project.name, project.budget, project.spent, project.saldo, project.status, project.kpi, new Date(project.deadline), project.comment])
//         .then(result => result.rows)
//         .then(rows => rows[0])
//         .then(row => row.id)
//         .then(id => res.status(201).json({projectId: id}))
//         .catch(err => {
//             console.error(err)
//             res.status(500).send({message: err.message})
//         });
//
// });
//
// router.post('/update', (req, res) => {
//     const {project} = req.body;
//     console.log('project:', project);
//     db.query('UPDATE project ' +
//         'SET name = $1, budget = $2, spent = $3, saldo = $4, status = $5, kpi = $6, deadline = $7, comment = $8 ' +
//         'WHERE id = $9',
//         [project.name, project.budget, project.spent, project.saldo, project.status, project.kpi,
//             new Date(project.deadline), project.comment, project.id])
//         .then(result => {
//             console.log(result);
//             return result;
//         })
//         .then(result => result.rows)
//         .catch(err => console.error(err));
// });
//
// router.get('/:id', (req, res) => {
//     const {id} = req.params;
//     db.query('SELECT * FROM project WHERE id = $1', [id])
//         .then(result => result.rows)
//         .then(rows => rows[0])
//         .then(row => {
//             console.log(row);
//             if (row) res.status(200).json(row);
//             else res.status(204).send({message: `Can not find project by id ${id}`});
//         })
//         .catch(err => {
//             console.error(err);
//             res.status(500).send({message: err.message});
//         });
// });
//
// router.get('/:id/name', (req, res) => {
//     db.query('SELECT DISTINCT * FROM project WHERE id = $1', [req.params.id])
//         .then(result => {
//             const {rows} = result;
//             if (rows && rows.length) res.json(rows[0]);
//             else res.status(204).send({message: `Can not find project by id ${req.params.id}`});
//         })
//         .catch(err => console.error(err));
// });

module.exports = router;