const router = require('express').Router();
const db = require('../../db');

// router.get('/', (req, res) => {
//     db.query('SELECT * FROM system_user')
//         .then(result => result.rows)
//         .then(rows => res.json(rows))
//         .catch(err => console.log(err));
// });

// router.put('/update', (req, res) => {
//     const {user} = req.body;
//
//     const errors = validateUser(user);
//     if (errors.length > 0) res.status(400).json(errors);
//
//     const {id, name, surname, email, phone} = user;
//     db.query('UPDATE system_user SET name=$1, surname=$2, email=$3, phone=$4 WHERE id=$5',
//         [name, surname, email, phone, id])
//         .then(rows => res.status(204).send())
//         .catch(err => res.status(500).json({message: err.message}));
// });

// router.get('/find/:id', (req, res) => {
//
//     db.query('SELECT DISTINCT * FROM system_user WHERE id=$1', [req.params.id])
//         .then(result => result.rows)
//         .then(rows => rows[0])
//         .then(row => res.status(200).json(row))
//         .catch(err => {
//             console.error(err)
//             res.status(500).send({message: err.message});
//         });
// });

function validateUser(user) {
    if (!user) throw Error('User parameter is null or undefined');

    const {name, surname, email, phone} = user;
    const errors = {name: [], surname: [], email: [], phone: []};

    if (name) errors.name.push('Name is empty');
    if (name) errors.name.push('Name is empty');
    if (name) errors.name.push('Name is empty');
    if (surname) errors.surname.push('Surname is empty');
    if (!email) errors.email.push('Email is empty');
    if (!phone) errors.phone.push('Phone is empty');

    console.log('errors found:', errors);
    const result = {};
    for (const key of Object.keys(errors)) {
        if (errors[key].length) {
            result[key] = [];
            for (const error of errors[key]) {
                result[key].push(error);
            }
        }
    }

    console.log('result:', result);
    return result;
}

module.exports = router;