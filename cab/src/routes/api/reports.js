const router = require('express').Router();
const db = require('../../db/index');

// router.get('/detailed', async (req, res) => {
//     const projectId = req.query.projectId;
//     const from = parseInt(req.query.from);
//     const to = parseInt(req.query.to);
//
//     try {
//         const dateFrom = new Date(from * 1000).toLocaleDateString().replace(/\./g, '\/');
//         const dateTo = new Date(to * 1000).toLocaleDateString().replace(/\./g, '\/');
//
//         const queryResult = await db.query(`SELECT empl.id,
//                                                    empl.name,
//                                                    empl.surname,
//                                                    inv.earned_money as earnedmoney,
//                                                    inv.hours
//                                             from involved as inv
//                                                      JOIN employee empl on inv.employee_id = empl.id
//                                             WHERE inv.report_id = $1
//                                               AND inv.day_id IN (
//                                                 SELECT id
//                                                 FROM day
//                                                 WHERE date BETWEEN $2 AND $3);`, [1, dateFrom, dateTo]);
//
//         const {rows} = queryResult;
//         if (!rows.length) res.status(204).json({message: `Days range for project ${projectId} is empty`});
//
//         const report = new Map();
//         rows.forEach(row => {
//             const {id: employeeId, name, surname, earnedmoney: earnedMoney, hours} = row;
//
//             if (report.has(employeeId)) {
//                 const worker = report.get(employeeId);
//
//                 report.set(employeeId, {
//                     ...worker,
//                     earnedMoney: worker.earnedMoney + earnedMoney,
//                     hours: worker.hours + hours
//                 });
//             } else {
//                 report.set(employeeId, {
//                     employeeId,
//                     name,
//                     surname,
//                     earnedMoney,
//                     hours,
//                 });
//             }
//         });
//
//         const result = [];
//         for (const value of report.values()) {
//             result.push(value)
//         }
//
//         res.json(result);
//     } catch (e) {
//         console.error(e);
//         res.status(500).send({message: e.message});
//     }
// });

// router.get('/detailed/kpi', async (req, res) => {
//     const from = parseInt(req.query.from);
//     const to = parseInt(req.query.to);
//     const {projectId, employeeId} = req.query;
//
//     try {
//         const dateFrom = new Date(from * 1000).toLocaleDateString().replace(/\./g, '\/');
//         const dateTo = new Date(to * 1000).toLocaleDateString().replace(/\./g, '\/');
//
//         const queryResult = await db.query(`SELECT empl.id,
//                                                    empl.name,
//                                                    empl.surname,
//                                                    inv.earned_money as earnedmoney,
//                                                    inv.hours,
//                                                    d.is_holiday     as isholiday
//                                             from involved as inv
//                                                      JOIN employee empl on inv.employee_id = empl.id
//                                                      JOIN day d on d.id = inv.day_id
//                                             WHERE inv.report_id = $1
//                                               AND inv.employee_id = $2
//                                               AND inv.day_id IN (
//                                                 SELECT id
//                                                 FROM day
//                                                 WHERE date BETWEEN $3 AND $4)`, [projectId, employeeId, dateFrom, dateTo]);
//
//         if (!queryResult.rows) return res.status(404).send({message: `Can not find project with id ${projectId}`});
//
//         const result = {
//             employeeId,
//             weekdays: {hours: 0, count: 0, earnedMoney: 0},
//             holidays: {hours: 0, count: 0, earnedMoney: 0}
//         };
//
//         const {rows} = queryResult;
//         rows.forEach(row => {
//             const {hours, earnedmoney: earnedMoney, isholiday: isHoliday} = row;
//             if (isHoliday) {
//                 const {holidays} = result;
//                 holidays.hours = holidays.hours + hours;
//                 holidays.count = holidays.count + 1;
//                 holidays.earnedMoney = holidays.earnedMoney + earnedMoney;
//             } else {
//                 const {weekdays} = result;
//                 weekdays.hours = weekdays.hours + hours;
//                 weekdays.count = weekdays.count + 1;
//                 weekdays.earnedMoney = weekdays.earnedMoney + earnedMoney;
//             }
//         });
//
//         res.json(result);
//     } catch (e) {
//         console.error(e);
//         res.status(500).send({message: e.message});
//     }
// });
//
// router.get('/general', async (req, res) => {
//     const from = parseInt(req.query.from);
//     const to = parseInt(req.query.to);
//
//     try {
//         const dateFrom = new Date(from * 1000).toLocaleDateString().replace(/\./g, '\/');
//         const dateTo = new Date(to * 1000).toLocaleDateString().replace(/\./g, '\/');
//
//         const queryResult = await db.query(`SELECT pr.id, pr.name, sum(inv.earned_money) as spent
//                                             FROM involved AS inv
//                                                      JOIN project pr on pr.id = inv.report_id
//                                             WHERE inv.report_id IN (
//                                                 SELECT id
//                                                 FROM report
//                                                 WHERE project_id = pr.id)
//                                               AND inv.day_id IN (
//                                                 SELECT id
//                                                 FROM day
//                                                 WHERE date BETWEEN $1 AND $2)
//                                             GROUP BY pr.id;`, [dateFrom, dateTo]);
//         if (!queryResult.rows) res.status(204).send({message: 'Can not find projects'});
//
//         res.json(queryResult.rows);
//     } catch (e) {
//         console.error(e);
//         res.status(500).send({message: e.message});
//     }
// });

module.exports = router;