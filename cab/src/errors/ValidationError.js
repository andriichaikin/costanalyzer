module.exports = class ValidationError extends Error {

    constructor(reason, ...params) {
        super(...params);

        this.name = 'ValidationError';
        this.reason = reason;
        this.date = new Date();

        if (Error.captureStackTrace) Error.captureStackTrace(this, ValidationError);

    }
}