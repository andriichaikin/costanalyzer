const db = require('../../db');

module.exports = {
    reportDetailedProject: args => {
        const projectId = args.projectId;
        const from = parseInt(args.from);
        const to = parseInt(args.to);

        const dateFrom = new Date(from * 1000).toLocaleDateString().replace(/\./g, '\/');
        const dateTo = new Date(to * 1000).toLocaleDateString().replace(/\./g, '\/');

        return db.query(`SELECT empl.id,
                                empl.name,
                                empl.surname,
                                inv.earned_money as earnedmoney,
                                inv.hours
                         from involved as inv
                                  JOIN employee empl on inv.employee_id = empl.id
                         WHERE inv.report_id = $1
                           AND inv.day_id IN (
                             SELECT id
                             FROM day
                             WHERE date BETWEEN $2 AND $3);`, [projectId, dateFrom, dateTo])
            .then(result => result.rows)
            .then(rows => {
                if (!rows.length) return [];

                const report = new Map();
                rows.forEach(row => {
                    const {id: employeeId, name, surname, earnedmoney: earnedMoney, hours} = row;

                    if (report.has(employeeId)) {
                        const worker = report.get(employeeId);

                        report.set(employeeId, {
                            ...worker,
                            earnedMoney: worker.earnedMoney + earnedMoney,
                            hours: worker.hours + hours
                        });
                    } else {
                        report.set(employeeId, {
                            employeeId,
                            name,
                            surname,
                            earnedMoney,
                            hours,
                        });
                    }
                });

                return {employees: report.values()};
            })
            .catch(err => new Error(err));
    },
    reportDetailedProjectEmployee: args => {
        const {dailyDetailedInput: input} = args;
        const from = parseInt(input.from);
        const to = parseInt(input.to);
        const {projectId, employeeId} = input;

        const dateFrom = new Date(from * 1000).toLocaleDateString().replace(/\./g, '\/');
        const dateTo = new Date(to * 1000).toLocaleDateString().replace(/\./g, '\/');

        //todo simplify query
        return db.query(`SELECT empl.id,
                                empl.name,
                                empl.surname,
                                inv.earned_money as earnedmoney,
                                inv.hours,
                                d.is_holiday     as isholiday
                         from involved as inv
                                  JOIN employee empl on inv.employee_id = empl.id
                                  JOIN day d on d.id = inv.day_id
                         WHERE inv.report_id = $1
                           AND inv.employee_id = $2
                           AND inv.day_id IN (
                             SELECT id
                             FROM day
                             WHERE date BETWEEN $3 AND $4)`, [projectId, employeeId, dateFrom, dateTo])
            .then(result => result.rows)
            .then(rows => {
                if (!rows.length) return [];

                const result = {
                    weekdays: {hours: 0, count: 0, earnedMoney: 0},
                    holidays: {hours: 0, count: 0, earnedMoney: 0}
                };

                rows.forEach(row => {
                    const {hours, earnedmoney: earnedMoney, isholiday: isHoliday} = row;
                    if (isHoliday) {
                        const {holidays} = result;
                        holidays.hours = holidays.hours + hours;
                        holidays.count = holidays.count + 1;
                        holidays.earnedMoney = holidays.earnedMoney + earnedMoney;
                    } else {
                        const {weekdays} = result;
                        weekdays.hours = weekdays.hours + hours;
                        weekdays.count = weekdays.count + 1;
                        weekdays.earnedMoney = weekdays.earnedMoney + earnedMoney;
                    }
                });
                return result;
            })
            .catch(err => new Error(err));
    },
    reportBriefProjects: args => {
        const from = parseInt(args.from);
        const to = parseInt(args.to);

        const dateFrom = new Date(from * 1000).toLocaleDateString().replace(/\./g, '\/');
        const dateTo = new Date(to * 1000).toLocaleDateString().replace(/\./g, '\/');

        return db.query(`SELECT pr.id, pr.name, sum(inv.earned_money) as spent
                         FROM involved AS inv
                                  JOIN project pr on pr.id = inv.report_id
                         WHERE inv.report_id IN (
                             SELECT id
                             FROM report
                             WHERE project_id = pr.id)
                           AND inv.day_id IN (
                             SELECT id
                             FROM day
                             WHERE date BETWEEN $1 AND $2)
                         GROUP BY pr.id;`, [dateFrom, dateTo])
            .then(result => result.rows)
            .catch(err => new Error(err));
    }
};