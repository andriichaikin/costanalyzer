const validateProject = require('../../services/projectService').validateProject;
const ValidationError = require('../../errors/ValidationError');

const db = require('../../db');

module.exports = {
    projects: args => {
        return db.query('SELECT * FROM project')
            .then(result => result.rows)
            .catch(err => new Error(err));
    },
    project: args => {
        const {id} = args;
        return db.query('SELECT * FROM project WHERE id=$1', [id])
            .then(result => result.rows)
            .then(rows => rows[0])
            .catch(err => new Error(err));
    },
    createProject: args => {
        const {name, budget, spent, saldo, status, kpi, deadline, comment} = args.projectInput;

        const errors = validateProject({name, budget, spent, saldo, status, kpi, deadline, comment});
        if (Object.keys(errors).length) throw new ValidationError(errors, 'Validation error');

        return db.query('INSERT INTO project (name, budget, spent, saldo, status, kpi, deadline, comment) ' +
            'VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *',
            [name, budget, spent, saldo, status, kpi, new Date(+deadline), comment])
            .then(result => result.rows)
            .then(rows => rows[0])
            .then(row => ({...row, deadline: row.deadline.getTime()}))
            .catch(err => new Error(err));
    },
    updateProject: args => {
        const {id, name, budget, spent, saldo, status, kpi, deadline, comment} = args.projectInput;

        const errors = validateProject({name, budget, spent, saldo, status, kpi, deadline, comment});
        if (Object.keys(errors).length) throw new ValidationError(errors, 'Validation error');

        return db.query('UPDATE project ' +
            'SET name=$1, budget=$2, spent=$3, saldo=$4, status=$5, kpi=$6, deadline=$7, comment=$8 ' +
            'WHERE id=$9 RETURNING *',
            [name, budget, spent, saldo, status, kpi, new Date(+deadline), comment, id])
            .then(result => result.rows)
            .then(rows => rows[0])
            .catch(err => new Error(err));
    }
};