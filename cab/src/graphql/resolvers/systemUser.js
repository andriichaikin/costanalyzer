const db = require('../../db');

module.exports = {
    systemUser: args => {
        return db.query('SELECT DISTINCT * FROM system_user WHERE id=$1', [args.id])
            .then(result => result.rows)
            .then(rows => rows[0])
            .catch(err => new Error(err));
    },
    updateSystemUser: args => {
        const {id, name, surname, email, phone} = args.systemUserInput;
        return db.query('UPDATE system_user ' +
            'SET name=$1, surname=$2, email=$3, phone=$4 ' +
            'WHERE id=$5 ' +
            'RETURNING id, name, surname, email, phone',
            [name, surname, email, phone, id])
            .then(result => result.rows)
            .then(rows => rows[0])
            .catch(err => new Error(err));
    }
};