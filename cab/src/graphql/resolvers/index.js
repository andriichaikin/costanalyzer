const db = require('../../db');

const systemUserResolver = require('./systemUser');
const employeeResolver = require('./employee');
const projectResolver = require('./project');
const projectReportResolver = require('./projectReport');

const rootResolver = {
    ...systemUserResolver,
    ...employeeResolver,
    ...projectResolver,
    ...projectReportResolver,
};
module.exports = {
    ...rootResolver
};