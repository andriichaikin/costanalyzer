const db = require('../../db');

module.exports = {
    employee: ({id}) => {
        return db.query('SELECT DISTINCT * FROM employee WHERE id=$1', [id])
            .then(result => result.rows)
            .then(rows => rows[0])
            .catch(err => new Error(err));
    },
    employees: () => {
        return db.query('SELECT * FROM employee')
            .then(result => result.rows)
            .catch(err => new Error(err));

    },
    createEmployee: (args) => {
        const {name, surname, position, salary} = args.employeeInput;

        return db.query('INSERT INTO employee (name, surname, position, salary) VALUES ($1, $2, $3, $4) RETURNING *',
            [name, surname, position, salary])
            .then(result => result.rows)
            .then(rows => rows[0])
            .catch(err => new Error(err));
    },
    updateEmployee: (args) => {
        const {id, name, surname, position, salary} = args.employeeInput;

        return db.query('UPDATE employee ' +
            'SET name=$1, surname=$2, position=$3, salary=$4 ' +
            'WHERE id=$5 ' +
            'RETURNING id, name, surname, position, salary',
            [name, surname, position, salary, id])
            .then(result => result.rows)
            .then(rows => rows[0])
            .catch(err => new Error(err));
    }
};