const {GraphQLObjectType, GraphQLSchema, GraphQLNonNull, GraphQLList, GraphQLInt} = require('graphql');

const {Employee, EmployeeCreateInput, EmployeeUpdateInput} = require('./types/employee');
const {SystemUser, SystemUserInput} = require('./types/systemUser');
const {Project, ProjectCreateInput, ProjectUpdateInput} = require('./types/project');
const {
    ReportBriefProject,
    ReportDetailedProject,
    ReportDetailedProjectEmployee,
    ReportDetailedProjectEmployeeInput
} = require('./types/report');

const rootQuery = new GraphQLObjectType({
    name: 'Query',
    fields: {
        employee: {
            type: Employee,
            args: {
                id: {type: new GraphQLNonNull(GraphQLInt)},
            }
        },
        employees: {
            type: new GraphQLList(Employee)
        },
        systemUser: {
            type: SystemUser,
            args: {
                id: {type: new GraphQLNonNull(GraphQLInt)},
            }
        },
        projects: {
            type: new GraphQLList(Project),
        },
        project: {
            type: Project,
            args: {
                id: {type: new GraphQLNonNull(GraphQLInt)},
            }
        },
        reportDetailedProject: {
            type: ReportDetailedProject,
            args: {
                projectId: {type: new GraphQLNonNull(GraphQLInt)},
                from: {type: new GraphQLNonNull(GraphQLInt)},
                to: {type: new GraphQLNonNull(GraphQLInt)},
            }
        },
        reportDetailedProjectEmployee: {
            type: ReportDetailedProjectEmployee,
            args: {
                dailyDetailedInput: {type: ReportDetailedProjectEmployeeInput},
            },
        },
        reportBriefProjects: {
            type: new GraphQLList(ReportBriefProject),
            args: {
                from: {type: new GraphQLNonNull(GraphQLInt)},
                to: {type: new GraphQLNonNull(GraphQLInt)},
            },
        },
    },
});

const rootMutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        createEmployee: {
            type: Employee,
            args: {
                employeeInput: {type: EmployeeCreateInput},
            },
        },
        updateEmployee: {
            type: Employee,
            args: {
                employeeInput: {type: EmployeeUpdateInput},
            },
        },
        updateSystemUser: {
            type: SystemUser,
            args: {
                systemUserInput: {type: SystemUserInput},
            },
        },
        createProject: {
            type: Project,
            args: {
                projectInput: {type: ProjectCreateInput},
            },
        },
        updateProject: {
            type: Project,
            args: {
                projectInput: {type: ProjectUpdateInput},
            },
        },
    },
});

const schema = new GraphQLSchema({
    query: rootQuery,
    mutation: rootMutation,
});

module.exports = schema;
