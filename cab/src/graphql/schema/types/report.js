const {
    GraphQLObjectType, GraphQLInputObjectType, GraphQLNonNull, GraphQLList, GraphQLString, GraphQLID, GraphQLInt
} = require('graphql');

const ReportDetailedEmployee = new GraphQLObjectType({
    name: 'ReportDetailedEmployee',
    fields: {
        employeeId: {type: new GraphQLNonNull(GraphQLInt)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        surname: {type: new GraphQLNonNull(GraphQLString)},
        earnedMoney: {type: new GraphQLNonNull(GraphQLInt)},
        hours: {type: new GraphQLNonNull(GraphQLInt)},
    },
});

const ReportDetailedProject = new GraphQLObjectType({
    name: 'ReportDetailedProject',
    fields: {
        employees: {type: new GraphQLList(ReportDetailedEmployee)},
    },
});

const DailySummary = new GraphQLObjectType({
    name: 'DailySummary',
    fields: {
        hours: {type: new GraphQLNonNull(GraphQLInt)},
        count: {type: new GraphQLNonNull(GraphQLInt)},
        earnedMoney: {type: new GraphQLNonNull(GraphQLInt)},
    },
});

const ReportDetailedProjectEmployee = new GraphQLObjectType({
    name: 'ReportDetailedProjectEmployee',
    fields: {
        weekdays: {type: DailySummary},
        holidays: {type: DailySummary},
    }
});

const ReportDetailedProjectEmployeeInput = new GraphQLInputObjectType({
    name: 'ReportDetailedProjectEmployeeInput',
    fields: {
        projectId: {type: new GraphQLNonNull(GraphQLInt)},
        employeeId: {type: new GraphQLNonNull(GraphQLInt)},
        from: {type: new GraphQLNonNull(GraphQLInt)},
        to: {type: new GraphQLNonNull(GraphQLInt)},
    }
});

const ReportBriefProject = new GraphQLObjectType({
    name: 'ReportProjectBrief',
    fields: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        spent: {type: new GraphQLNonNull(GraphQLInt)},
    }
});

module.exports = {
    ReportBriefProject,
    ReportDetailedProject,
    ReportDetailedProjectEmployee,
    ReportDetailedProjectEmployeeInput
};