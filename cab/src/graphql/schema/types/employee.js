const {
    GraphQLObjectType, GraphQLInputObjectType, GraphQLNonNull, GraphQLString, GraphQLID, GraphQLInt
} = require('graphql');

const Employee = new GraphQLObjectType({
    name: 'Employee',
    fields: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        surname: {type: new GraphQLNonNull(GraphQLString)},
        position: {type: new GraphQLNonNull(GraphQLString)},
        salary: {type: new GraphQLNonNull(GraphQLInt)},
    }
});

const EmployeeCreateInput = new GraphQLInputObjectType({
    name: 'EmployeeCreateInput',
    fields: {
        name: {type: new GraphQLNonNull(GraphQLString)},
        surname: {type: new GraphQLNonNull(GraphQLString)},
        position: {type: new GraphQLNonNull(GraphQLString)},
        salary: {type: new GraphQLNonNull(GraphQLInt)},
    }

});
const EmployeeUpdateInput = new GraphQLInputObjectType({
    name: 'EmployeeUpdateInput',
    fields: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        surname: {type: new GraphQLNonNull(GraphQLString)},
        position: {type: new GraphQLNonNull(GraphQLString)},  //todo enum
        salary: {type: new GraphQLNonNull(GraphQLInt)},
    }
});

module.exports = {Employee, EmployeeCreateInput, EmployeeUpdateInput};