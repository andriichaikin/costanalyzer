const {
    GraphQLObjectType, GraphQLInputObjectType, GraphQLNonNull, GraphQLString, GraphQLID, GraphQLInt
} = require('graphql');


const Project = new GraphQLObjectType({
    name: 'Project',
    fields: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        budget: {type: new GraphQLNonNull(GraphQLInt)},
        spent: {type: new GraphQLNonNull(GraphQLInt)},
        saldo: {type: new GraphQLNonNull(GraphQLInt)},
        status: {type: new GraphQLNonNull(GraphQLString)},
        kpi: {type: GraphQLInt},
        deadline: {type: new GraphQLNonNull(GraphQLString)},
        comment: {type: GraphQLString},
    },
});

const ProjectCreateInput = new GraphQLInputObjectType({
    name: 'ProjectCreateInput',
    fields: {
        name: {type: new GraphQLNonNull(GraphQLString)},
        budget: {type: new GraphQLNonNull(GraphQLInt)},
        spent: {type: new GraphQLNonNull(GraphQLInt)},
        saldo: {type: new GraphQLNonNull(GraphQLInt)},
        status: {type: new GraphQLNonNull(GraphQLString)},
        kpi: {type: GraphQLInt},
        deadline: {type: new GraphQLNonNull(GraphQLString)},
        comment: {type: GraphQLString},
    },
});

const ProjectUpdateInput = new GraphQLInputObjectType({
    name: 'ProjectUpdateInput',
    fields: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        budget: {type: new GraphQLNonNull(GraphQLInt)},
        spent: {type: new GraphQLNonNull(GraphQLInt)},
        saldo: {type: new GraphQLNonNull(GraphQLInt)},
        status: {type: new GraphQLNonNull(GraphQLString)},
        kpi: {type: GraphQLInt},
        deadline: {type: new GraphQLNonNull(GraphQLString)},
        comment: {type: GraphQLString},
    },
});

module.exports = {Project, ProjectCreateInput, ProjectUpdateInput};