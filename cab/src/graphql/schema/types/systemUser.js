const {
    GraphQLObjectType, GraphQLInputObjectType, GraphQLNonNull, GraphQLInt, GraphQLString, GraphQLID
} = require('graphql');

const SystemUser = new GraphQLObjectType({
    name: 'SystemUser',
    fields: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        surname: {type: new GraphQLNonNull(GraphQLString)},
        email: {type: new GraphQLNonNull(GraphQLString)},
        password: {type: new GraphQLNonNull(GraphQLString)},
        phone: {type: new GraphQLNonNull(GraphQLString)},
    }
});

const SystemUserInput = new GraphQLInputObjectType({
    name: 'SystemUserInput',
    fields: {
        id: {type: new GraphQLNonNull(GraphQLInt)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        surname: {type: new GraphQLNonNull(GraphQLString)},
        email: {type: new GraphQLNonNull(GraphQLString)},
        phone: {type: new GraphQLNonNull(GraphQLString)},
    }
});

module.exports = {SystemUser, SystemUserInput};