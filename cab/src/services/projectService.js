const STATUS_LIST = ['OPEN', 'CLOSED', 'FREEZED'];

function validateProject(project) {
    if (!project) throw Error('Parameter is null or undefined');

    const {name, budget, spent, saldo, status, deadline} = project;
    const errors = {
        name: [],
        budget: [],
        spent: [],
        saldo: [],
        // kpi  //todo kpi
        status: [],
        deadline: [],
    };

    if (!name) errors.name.push('Name is empty');

    if (budget === undefined) errors.budget.push['Budget not assigned'];
    if (!Number.isInteger(budget)) errors.budget.push('Budget is not an integer');
    if (budget <= 0) errors.budget.push('Budget can not be zero or negative');

    if (spent === undefined) errors.spent.push('Spent not assigned');
    if (!Number.isInteger(spent)) errors.spent.push('Spent is not an integer');
    if (spent < 0) errors.spent.push('Spent can not by negative integer');

    if (errors.budget.length === 0 || errors.spent.length === 0) {
        const saldoCalculated = budget - spent;

        if (!Number.isInteger(saldo)) errors.saldo.push('Saldo is not an integer');
        if (saldo !== saldoCalculated) errors.saldo.push('Saldo is not calculated correctly');
    } else {
        errors.saldo.push['Can not calculate saldo. Check budget and spent integers'];
    }

    if (!status) errors.status.push('Status not assigned');
    if (status && !STATUS_LIST.includes(status)) errors.status.push('Can not determine status');

    if (deadline) {
        let date = new Date(+deadline);
        if (!date instanceof Date || isNaN(date)) errors.deadline.push('Incorrect deadline date');
    } else {
        errors.deadline.push('Deadline not assigned');
    }

    console.log('found errors:', errors);
    const result = {};
    Object.keys(errors).forEach(key => {
        if (errors[key].length) {
            result[key] = [];
            errors[key].forEach(e => result[key].push(e));
        }
    });
    return result;
}

function prepareProject(project) {

    return project;
}

module.exports = {
    validateProject,
    prepareProject,
};