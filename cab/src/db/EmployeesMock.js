const employees = [
    {
        id: 44441,
        name: 'John',
        surname: 'Smith',
        position: 'engineer',
        salary: 10000,
    },
    {
        id: 44442,
        name: 'Samuel',
        surname: 'Baggins',
        position: 'engineer',
        salary: 10000,
    },
    {
        id: 44443,
        name: 'Ol',
        surname: 'Olofson',
        position: 'engineer',
        salary: 10000,
    },
];

module.exports = employees;