const projects = [
    {
        id: 11111,
        name: 'Project 1',
        budget: 100,
        spent: 20,
        balance: 80808080,
        forecast_1: 111,
        forecast_2: 222,
        status: 'OPEN',
        kpi: 1.2,
        deadLine: '12-12-2012',
        comment: 'Description DescriptionDescription Description DescriptionDescription DescriptionDescription Description DescriptionDescription DescriptionDescription Description Description',
        report:
            {
                id: 22221,
                days: [
                    {
                        id: 33331,
                        date: 1577664000000,
                        day: 'MONDAY',
                        isHoliday: false,
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33332,
                        date: 1577750400000,
                        day: 'TUESDAY',
                        isHoliday: false,
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33333,
                        date: 1577836800000,
                        day: 'WEDNESDAY',
                        isHoliday: false,
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33334,
                        date: 1577923200000,
                        day: 'THURSDAY',
                        isHoliday: false,
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33335,
                        date: 1578009600000,
                        day: 'FRIDAY',
                        isHoliday: false,
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33336,
                        date: 1578096000000,
                        day: 'SATURDAY',
                        isHoliday: true,
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33337,
                        date: 1578182400000,
                        day: 'SUNDAY',
                        isHoliday: true,
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                ],
            },
    },
    {
        id: 11112,
        name: 'Pr 2',
        budget: 100,
        spent: 20,
        balance: 80,
        forecast_1: 111,
        forecast_2: 222,
        status: 'OPEN',
        kpi: 1.2,
        deadLine: '12-12-2012',
        comment: 'Description',
        report:
            {
                id: 22222,
                days: [
                    {
                        id: 33331,
                        date: 1577664000000,
                        day: 'MONDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33332,
                        date: 1577750400000,
                        day: 'TUESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33333,
                        date: 1577836800000,
                        day: 'WEDNESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33334,
                        date: 1577923200000,
                        day: 'THURSDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33335,
                        date: 1578009600000,
                        day: 'FRIDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33336,
                        date: 1578096000000,
                        day: 'SATURDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33337,
                        date: 1578182400000,
                        day: 'SUNDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                ],
            },
    },
    {
        id: 11113,
        name: 'Pr 3',
        budget: 100,
        spent: 20,
        balance: 80,
        forecast_1: 111,
        forecast_2: 222,
        status: 'OPEN',
        kpi: 1.2,
        deadLine: '12-12-2012',
        comment: 'Description',
        report:
            {
                id: 22223,
                days: [
                    {
                        id: 33331,
                        date: 1577664000000,
                        day: 'MONDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33332,
                        date: 1577750400000,
                        day: 'TUESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33333,
                        date: 1577836800000,
                        day: 'WEDNESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33334,
                        date: 1577923200000,
                        day: 'THURSDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33335,
                        date: 1578009600000,
                        day: 'FRIDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33336,
                        date: 1578096000000,
                        day: 'SATURDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33337,
                        date: 1578182400000,
                        day: 'SUNDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                ],
            },
    },
    {
        id: 11114,
        name: 'Pr 4',
        budget: 100,
        spent: 20,
        balance: 80,
        forecast_1: 111,
        forecast_2: 222,
        status: 'OPEN',
        kpi: 1.2,
        deadLine: '12-12-2012',
        comment: 'Description',
        report:
            {
                id: 22224,
                days: [
                    {
                        id: 33331,
                        date: 1577664000000,
                        day: 'MONDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33332,
                        date: 1577750400000,
                        day: 'TUESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33333,
                        date: 1577836800000,
                        day: 'WEDNESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33334,
                        date: 1577923200000,
                        day: 'THURSDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33335,
                        date: 1578009600000,
                        day: 'FRIDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33336,
                        date: 1578096000000,
                        day: 'SATURDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33337,
                        date: 1578182400000,
                        day: 'SUNDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                ],
            },
    },
    {
        id: 11115,
        name: 'Pr 5',
        budget: 100,
        spent: 20,
        balance: 80,
        forecast_1: 111,
        forecast_2: 222,
        status: 'OPEN',
        kpi: 1.2,
        deadLine: '12-12-2012',
        comment: 'Description',
        report:
            {
                id: 22225,
                days: [
                    {
                        id: 33331,
                        date: 1577664000000,
                        day: 'MONDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33332,
                        date: 1577750400000,
                        day: 'TUESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33333,
                        date: 1577836800000,
                        day: 'WEDNESDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33334,
                        date: 1577923200000,
                        day: 'THURSDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33335,
                        date: 1578009600000,
                        day: 'FRIDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33336,
                        date: 1578096000000,
                        day: 'SATURDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                    {
                        id: 33337,
                        date: 1578182400000,
                        day: 'SUNDAY',
                        involved: [
                            {
                                employeeId: 44441,
                                earnedMoney: 200,
                                hours: 8,
                            },
                            {
                                employeeId: 44442,
                                earnedMoney: 200,
                                hours: 2,
                            },
                            {
                                employeeId: 44443,
                                earnedMoney: 200,
                                hours: 3,
                            },
                        ],
                    },
                ],
            },
    },
];

module.exports = projects;