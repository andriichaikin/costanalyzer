const express = require('express');
const graphqlHTTP = require('express-graphql');
const mountRoutes = require('./routes/index');

const cors = require('cors');
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 5000;
const app = express();
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://192.168.0.112:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Credentials", true);
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});

// const corsConfiguration = {
//     origin: "http://192.168.0.112:3000",
//     credentials: true,
//     methods: ['GET', 'POST', 'PUT', 'OPTIONS'],
//     allowedHeaders: 'Content-Type, Origin, X-Requested-With, Content-Type, Accept',
//     optionsSuccessStatus: 204,
// };
// app.use(cors(corsConfiguration));

const schema = require('./graphql/schema/index');
const rootResolver = require('./graphql/resolvers/index');
app.use(
    '/api/graphql',
    graphqlHTTP({
        schema: schema,
        rootValue: rootResolver,
        graphiql: true,
        formatError: err => {

            if (err.originalError && err.originalError.reason)
                return ({
                    message: err.message,
                    statusCode: err.statusCode,
                    reason: err.originalError.reason
                });

            return ({
                message: err.message,
                statusCode: err.statusCode,
            });
        }
    })
);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

// mountRoutes(app);


app.listen(PORT, () => console.log(`Server started on port ${PORT}`));