
INSERT INTO role (role) values ('ADMIN');
INSERT INTO role (role) values ('USER');

INSERT INTO system_user (name, surname, email, password, phone)
VALUES ('adminName', 'adminSurname', 'admin@gmail.com', 'adminPass','+380991231234');

INSERT INTO system_user_role (system_user_id, role_id)
SELECT r.id, sys_usr.id
from role r
JOIN system_user sys_usr
ON r.role = 'ADMIN' AND sys_usr.email = 'admin@gmail.com';

INSERT INTO system_user (name, surname, email, password, phone)
VALUES ('userName', 'userSurname', 'user@gmail.com', 'userPass','+380991234321');

INSERT INTO system_user_role (system_user_id, role_id)
SELECT r.id, sys_usr.id
from role r
JOIN system_user sys_usr
ON r.role = 'USER' AND sys_usr.email = 'user@gmail.com';

INSERT INTO employee (name, surname, position, salary) VALUES ('John', 'Doe', 'engineer', 10000);
INSERT INTO employee (name, surname, position, salary) VALUES ('Ol', 'Olofson', 'engineer', 12000);
INSERT INTO employee (name, surname, position, salary) VALUES ('Mike', 'Smith', 'engineer', 15500);
INSERT INTO employee (name, surname, position, salary) VALUES ('Dmitrii', 'Cooper', 'engineer', 18000);
INSERT INTO employee (name, surname, position, salary) VALUES ('Tony', 'Star', 'engineer', 22500);


INSERT INTO day (date, day, is_holiday) VALUES ('2/1/2020', 'SATURDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/2/2020', 'SUNDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/3/2020', 'MONDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/4/2020', 'TUESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/5/2020', 'WEDNESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/6/2020', 'THURSDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/7/2020', 'FRIDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/8/2020', 'SATURDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/9/2020', 'SUNDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/10/2020', 'MONDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/11/2020', 'TUESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/12/2020', 'WEDNESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/13/2020', 'THURSDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/14/2020', 'FRIDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/15/2020', 'SATURDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/15/2020', 'SUNDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/16/2020', 'MONDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/17/2020', 'TUESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/18/2020', 'WEDNESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/19/2020', 'THURSDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/20/2020', 'FRIDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/21/2020', 'SATURDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/22/2020', 'SUNDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/23/2020', 'MONDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/24/2020', 'TUESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/25/2020', 'WEDNESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/26/2020', 'THURSDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/27/2020', 'FRIDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('2/28/2020', 'SATURDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('2/29/2020', 'SUNDAY', true);
INSERT INTO day (date, day, is_holiday) VALUES ('3/1/2020', 'MONDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('3/2/2020', 'TUESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('3/3/2020', 'WEDNESDAY', false);
INSERT INTO day (date, day, is_holiday) VALUES ('3/4/2020', 'THURSDAY', false);



-- INSERT INTO forecast (project_id)
-- SELECT pr.id
-- from project pr
-- where pr.name = 'Budivelnik Corp';



INSERT INTO project (name, budget, spent, saldo, status, kpi, deadline, comment)
VALUES ('Budivelnik Corp', 1000000, 200000, 800000, 'OPEN', 1.2, '1/3/2021', 'Project Budivelnik description');

INSERT INTO report (project_id)
SELECT pr.id
from project pr
where pr.name = 'Budivelnik Corp';


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));



INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Budivelnik Corp')));












INSERT INTO project (name, budget, spent, saldo, status, kpi, deadline, comment)
VALUES ('Art house building', 1000000, 200000, 800000, 'OPEN', 1.2, '10/10/2020', 'Art house building description');

INSERT INTO report (project_id)
SELECT pr.id
from project pr
where pr.name = 'Art house building';


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/1/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));



INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/2/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/3/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/4/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/5/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/6/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/7/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));


INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (400, 2,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'John'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (800, 4,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Ol'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (500, 4,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Mike'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (1200, 8,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Dmitrii'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));

INSERT INTO involved (earned_money, hours, day_id, employee_id, report_id)
VALUES (700, 1,
        (SELECT id FROM day WHERE date = '2/8/2020'),
        (SELECT id FROM employee WHERE name = 'Tony'),
        (SELECT id FROM report WHERE project_id IN (SELECT id FROM project WHERE name = 'Art house building')));